<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_events', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('matches_id');
            $table->integer('teams_id');
            $table->integer('players_id');
            $table->integer('match_event_minute');
            $table->integer('match_event_types_id');
            $table->string('match_event_notes',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match_events');
    }
}
