<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('match_label',100);
            $table->integer('teams_id_host');
            $table->integer('teams_id_guest');
            $table->integer('tournaments_id')->nullable();
            $table->date('match_day')->nullable();
            $table->integer('time_frames_id')->nullable();
            $table->integer('fields_id')->nullable();
            $table->string('match_status',100)->default('Da giocare');
            $table->integer('referees_id')->nullable();        
            $table->integer('teams_id_host_goals')->default(0);
            $table->integer('teams_id_guest_goals')->default(0);
            $table->integer('teams_id_winner')->nullable();
            $table->integer('delegates_id')->nullable();    
            $table->integer('match_turn')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
