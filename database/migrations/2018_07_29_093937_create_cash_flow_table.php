<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashFlowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cfos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('cfo_label',100)->nullable();
            $table->integer('cfo_sign');
            $table->float('cfo_value')->nullable();
            $table->integer('cfo_types_id');
            $table->date('cfo_date')->nullable();
            $table->integer('cfo_confirmed');
            $table->integer('teams_id')->nullable();
            $table->integer('players_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cfos');
    }
}
