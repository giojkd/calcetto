<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentCategoryCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournament_category_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('tournaments_id');
            $table->integer('team_categories_id');
            $table->float('tournament_category_cost_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournament_category_costs');
    }
}
