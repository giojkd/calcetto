<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('fields_id');
            $table->integer('time_frames_id');
            $table->integer('team_types_id');
            $table->date('match_day');
            $table->string('name');
            $table->string('surname');
            $table->string('telephone');
            $table->string('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields_reservations');
    }
}
