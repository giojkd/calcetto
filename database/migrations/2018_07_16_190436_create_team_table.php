<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('team_name',100);
            $table->string('team_president_first_name',100)->nullable();
            $table->string('team_president_last_name',100)->nullable();
            $table->date('team_date_of_establishing');
            $table->string('team_logo')->nullable();
            $table->integer('team_categories_id');
            $table->integer('team_kinds_id'); #calcio a 5, a 7 etc..
            $table->integer('team_types_id'); #maschile, femminile etc..
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
