<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTeams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches_teams', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('matches_id');
            $table->integer('teams_id');
            $table->integer('points')->default(0);
            $table->string('matches_teams_mode',100);
            $table->float('scores_in')->default(0);#conceded
            $table->float('scores_out')->default(0);#scored
            $table->string('result')->nullable();#win lose draw
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches_teams');
    }
}
