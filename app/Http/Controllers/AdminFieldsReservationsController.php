<?php namespace App\Http\Controllers;

use Session;
#use Request;
use DB;
use CRUDBooster;
use Illuminate\Http\Request;

class AdminFieldsReservationsController extends \crocodicstudio\crudbooster\controllers\CBController {

	public function cbInit() {

		# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "name";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = false;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = false;
		$this->button_export = true;
		$this->table = "fields_reservations";
		# END CONFIGURATION DO NOT REMOVE THIS LINE

		# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"Gioco","name"=>"team_types_id","join"=>"team_types,team_type_name"];
		$this->col[] = ["label"=>"Giorno","name"=>"match_day"];
		$this->col[] = ["label"=>"Campo","name"=>"fields_id","join"=>"fields,field_name"];

		$this->col[] = ["label"=>"Orario","name"=>"time_frames_id","join"=>"time_frames,time_frame_label"];
		$this->col[] = ["label"=>"Importo","name"=>"fields_id","join"=>"fields,field_cost"];

		$this->col[] = ["label"=>"Nome","name"=>"name"];
		$this->col[] = ["label"=>"Cognome","name"=>"surname"];
		$this->col[] = ["label"=>"Telefono","name"=>"telephone"];
		$this->col[] = ["label"=>"Email","name"=>"email"];
		# END COLUMNS DO NOT REMOVE THIS LINE

		# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
		$this->form[] = ['label'=>'Campo','name'=>'fields_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'fields,field_name'];
		$this->form[] = ['label'=>'Orario','name'=>'time_frames_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'time_frames,time_frame_label'];
		$this->form[] = ['label'=>'Gioco','name'=>'team_types_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'team_types,team_type_name'];
		$this->form[] = ['label'=>'Giorno','name'=>'match_day','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Nome','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'Puoi inserire soltanto lettere'];
		$this->form[] = ['label'=>'Cognome','name'=>'surname','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Telefono','name'=>'telephone','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:fields_reservations','width'=>'col-sm-10','placeholder'=>'Per favore inserisci un indirizzo email valido'];
		# END FORM DO NOT REMOVE THIS LINE

		# OLD START FORM
		//$this->form = [];
		//$this->form[] = ['label'=>'Campo','name'=>'fields_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'fields,field_name'];
		//$this->form[] = ['label'=>'Orario','name'=>'time_frames_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'time_frames,time_frame_label'];
		//$this->form[] = ['label'=>'Gioco','name'=>'team_types_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'team_types,team_type_name'];
		//$this->form[] = ['label'=>'Giorno','name'=>'match_day','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Nome','name'=>'name','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'Puoi inserire soltanto lettere'];
		//$this->form[] = ['label'=>'Cognome','name'=>'surname','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Telefono','name'=>'telephone','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		//$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:fields_reservations','width'=>'col-sm-10','placeholder'=>'Per favore inserisci un indirizzo email valido'];
		# OLD END FORM

		/*
		| ----------------------------------------------------------------------
		| Sub Module
		| ----------------------------------------------------------------------
		| @label          = Label of action
		| @path           = Path of sub module
		| @foreign_key 	  = foreign key of sub table/module
		| @button_color   = Bootstrap Class (primary,success,warning,danger)
		| @button_icon    = Font Awesome Class
		| @parent_columns = Sparate with comma, e.g : name,created_at
		|
		*/
		$this->sub_module = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Action Button / Menu
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
		| @icon        = Font awesome class icon. e.g : fa fa-bars
		| @color 	   = Default is primary. (primary, warning, succecss, info)
		| @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
		|
		*/
		$this->addaction = array();


		/*
		| ----------------------------------------------------------------------
		| Add More Button Selected
		| ----------------------------------------------------------------------
		| @label       = Label of action
		| @icon 	   = Icon from fontawesome
		| @name 	   = Name of button
		| Then about the action, you should code at actionButtonSelected method
		|
		*/
		$this->button_selected = array();


		/*
		| ----------------------------------------------------------------------
		| Add alert message to this module at overheader
		| ----------------------------------------------------------------------
		| @message = Text of message
		| @type    = warning,success,danger,info
		|
		*/
		$this->alert        = array();



		/*
		| ----------------------------------------------------------------------
		| Add more button to header button
		| ----------------------------------------------------------------------
		| @label = Name of button
		| @url   = URL Target
		| @icon  = Icon from Awesome.
		|
		*/
		$this->index_button = array();



		/*
		| ----------------------------------------------------------------------
		| Customize Table Row Color
		| ----------------------------------------------------------------------
		| @condition = If condition. You may use field alias. E.g : [id] == 1
		| @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
		|
		*/
		$this->table_row_color = array();


		/*
		| ----------------------------------------------------------------------
		| You may use this bellow array to add statistic at dashboard
		| ----------------------------------------------------------------------
		| @label, @count, @icon, @color
		|
		*/
		$this->index_statistic = array();



		/*
		| ----------------------------------------------------------------------
		| Add javascript at body
		| ----------------------------------------------------------------------
		| javascript code in the variable
		| $this->script_js = "function() { ... }";
		|
		*/
		$this->script_js = NULL;


		/*
		| ----------------------------------------------------------------------
		| Include HTML Code before index table
		| ----------------------------------------------------------------------
		| html code to display it before index table
		| $this->pre_index_html = "<p>test</p>";
		|
		*/
		ob_start();
		/*echo '<pre>';
		print_r($_GET);
		echo '</pre>';*/
		$fields = DB::table('fields')->get();
		$_GET['filter_column']['matches.match_day']['value'][0] = ($_GET['filter_column']['fields_reservations.match_day']['value'][0]) ? : date('Y-m-d');
		$_GET['filter_column']['matches.match_day']['value'][1] = ($_GET['filter_column']['fields_reservations.match_day']['value'][1]) ? : date('Y-m-d',strtotime('+7days'));
		?>
		<form class="form form-inline" method="/admin/matches">
			<input type="hidden" name="filter_column[fields_reservations.match_day][type]" value="between">
			<input type="hidden" name="filter_column[fields.field_name][type]" value="like">

			<div class="form-group">
				<label>Campo:</label>
				<!--filter_column[fields.field_name][value]-->
				<select name="filter_column[fields.field_name][value]" id="" class="form-control">
					<option value="">Scegli...</option>
					<?php foreach($fields as $field){?>
						<option <?php echo ($_GET['filter_column']['fields.field_name']['value']==$field->field_name) ? 'selected' : ''?> value="<?php echo $field->field_name?>"><?php echo $field->field_name?></option>
					<?php }?>
				</select>
			</div>
			<div class="form-group">
				<label>Da:</label>
				<input value="<?php echo($_GET['filter_column']['fields_reservations.match_day']['value'][0]) ?>" name="filter_column[fields_reservations.match_day][value][0]" type="text" class="form-control datepicker">
			</div>
			<div class="form-group">
				<label>A:</label>
				<input value="<?php echo($_GET['filter_column']['fields_reservations.match_day']['value'][1]) ?>" name="filter_column[fields_reservations.match_day][value][1]" type="text" class="form-control datepicker">
			</div>
			<button style="margin-top:24px" type="submit" class="btn btn-sm btn-success">Filtra</button>
		</form>
		<?php
		$cane = ob_get_clean();
		$this->pre_index_html = $cane;




		/*
		| ----------------------------------------------------------------------
		| Include HTML Code after index table
		| ----------------------------------------------------------------------
		| html code to display it after index table
		| $this->post_index_html = "<p>test</p>";
		|
		*/
		$this->post_index_html = null;



		/*
		| ----------------------------------------------------------------------
		| Include Javascript File
		| ----------------------------------------------------------------------
		| URL of your javascript each array
		| $this->load_js[] = asset("myfile.js");
		|
		*/
		$this->load_js = array();



		/*
		| ----------------------------------------------------------------------
		| Add css style at body
		| ----------------------------------------------------------------------
		| css code in the variable
		| $this->style_css = ".style{....}";
		|
		*/
		$this->style_css = NULL;



		/*
		| ----------------------------------------------------------------------
		| Include css File
		| ----------------------------------------------------------------------
		| URL of your css each array
		| $this->load_css[] = asset("myfile.css");
		|
		*/
		$this->load_css = array();


	}

	public function getAdd(){
		//Create an Auth
		if(!CRUDBooster::isCreate() && $this->global_privilege==FALSE || $this->button_add==FALSE) {
			CRUDBooster::redirect(CRUDBooster::adminPath(),trans("crudbooster.denied_access"));
		}

		$data = [];
		$data['page_title'] = 'Add Data';

		$teamTypes = DB::table('team_types')->get();

		$data['teamTypes'] = $teamTypes;


		//Please use cbView method instead view method from laravel
		$this->cbView('fields_reservation_custom_add_view',$data);
	}

	public function postCheckAvailability(Request $request){
		$data = $_POST;
		$weeksCounts = 0;
		$firstDay = $data['from'];
		$busyDays = ['busyDays' => []];
		while(strtotime($firstDay.'+'.$weeksCounts.'weeks')<=strtotime($data['until'])){

			$match_day = date('Y-m-d',strtotime($firstDay.'+'.$weeksCounts.'weeks'));

			$check = DB::table('matches')->where('match_day',$match_day)->where('fields_id',$data['fields_id'])->where('time_frames_id',$data['time_frames_id'])->exists();
			if($check){
				$busyDays['busyDays'][] = $match_day;
			}
			$weeksCounts++;
		}
		return $busyDays;
	}

	public function postAddSave(){

#		$request = new Request();

		$data = $_POST;
		unset($data['_token']);
		unset($data['field_option']);

		$data['created_at'] = date('Y-m-d');

		if($data['reservation_type'] == 'single'){

			$match = [
				'match_day' => $data['match_day'],
				'time_frames_id' => $data['time_frames_id'],
				'fields_id' => $data['fields_id'],
				'match_label' => 'Campo prenotato'
			];
			$matches_id = DB::table('matches')
			->insertGetId($match);
			$data['matches_id'] = $matches_id;
			$fields_reservations_id = DB::table('fields_reservations')
			->insertGetId($data);



		}

		if($data['reservation_type'] == 'recursive'){
			$weeksCounts = 0;
			$firstDay = $data['match_day'];
			while(strtotime($firstDay.'+'.$weeksCounts.'weeks')<=strtotime($data['reservation_until'])){

				$data['match_day'] = date('Y-m-d',strtotime($firstDay.'+'.$weeksCounts.'weeks'));
				$match = [
					'match_day' => $data['match_day'],
					'time_frames_id' => $data['time_frames_id'],
					'fields_id' => $data['fields_id'],
					'match_label' => 'Campo prenotato'
				];
				$matches_id = DB::table('matches')
				->insertGetId($match);
				$data['matches_id'] = $matches_id;
				$fields_reservations_id = DB::table('fields_reservations')
				->insertGetId($data);

				$weeksCounts++;
			}
		}

		CRUDBooster::redirect(CRUDBooster::mainpath(),'Salvataggio ok','success');
		#header('location:/admin/fields_reservations');
exit;
	}

	/*
	| ----------------------------------------------------------------------
	| Hook for button selected
	| ----------------------------------------------------------------------
	| @id_selected = the id selected
	| @button_name = the name of button
	|
	*/
	public function actionButtonSelected($id_selected,$button_name) {
		//Your code here

	}


	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate query of index result
	| ----------------------------------------------------------------------
	| @query = current sql query
	|
	*/
	public function hook_query_index(&$query) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate row of index table html
	| ----------------------------------------------------------------------
	|
	*/
	public function hook_row_index($column_index,&$column_value) {
		//Your code here
	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate data input before add data is execute
	| ----------------------------------------------------------------------
	| @arr
	|
	*/
	public function hook_before_add(&$postdata) {
		//Your code here
		$match = [
			'match_day' => $postdata['match_day'],
			'time_frames_id' => $postdata['time_frames_id'],
			'fields_id' => $postdata['fields_id'],
			'match_label' => 'Campo prenotato'
		];

		$postdata['matches_id'] = DB::table('matches')
		->insertGetId($match);

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after add public static function called
	| ----------------------------------------------------------------------
	| @id = last insert id
	|
	*/
	public function hook_after_add($id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for manipulate data input before update data is execute
	| ----------------------------------------------------------------------
	| @postdata = input post data
	| @id       = current id
	|
	*/
	public function hook_before_edit(&$postdata,$id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after edit public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_after_edit($id) {
		//Your code here

	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command before delete public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_before_delete($id) {
		//Your code here
		$matches_id = DB::table('fields_reservations')->where('id',$id)->value('matches_id');


		DB::table('matches')->where('id',$matches_id)->delete();
	}

	/*
	| ----------------------------------------------------------------------
	| Hook for execute command after delete public static function called
	| ----------------------------------------------------------------------
	| @id       = current id
	|
	*/
	public function hook_after_delete($id) {
		//Your code here


	}



	//By the way, you can still create your own method in here... :)


}
