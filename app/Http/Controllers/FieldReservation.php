<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class FieldReservation extends Controller
{

  public function newFieldReservation(Request $request){
    $data = $request->all();





    $match = [
      'match_day' => $data['match_day'],
      'time_frames_id' => $data['time_frames_id'],
      'fields_id' => $data['fields_id'],
      'match_label' => 'Campo prenotato'
    ];

    $matches_id = DB::table('matches')
    ->insertGetId($match);
    $data['matches_id'] = $matches_id;
    $fields_reservations_id = DB::table('fields_reservations')
    ->insertGetId($data);
    $return['fields_reservations_id'] = $fields_reservations_id;
    echo json_encode($return);
  }

  public function getGames(){
    return DB::table('team_types')->get();
  }

  public function getFields2(Request $request){
    $query = "SELECT
    f.field_name,
    f.id as fields_id,
    f.field_cost,
    (SELECT company_address FROM companies c WHERE c.id = f.companies_id) as field_address,
    (SELECT COUNT(*) FROM matches m WHERE m.fields_id = f.id AND m.time_frames_id = tf2.id AND m.match_day = td.db_date) as count_matches,
    tf.time_frames_id,
    tf2.time_frame_label
    FROM fields f
    LEFT JOIN fields_team_types_table fttt ON f.id = fttt.fields_id
    LEFT JOIN tournaments_fields tf ON f.id = tf.fields_id
    LEFT JOIN time_dimension td ON td.day_id = tf.days_id
    LEFT JOIN time_frames tf2 ON tf2.id = tf.time_frames_id

    WHERE
    fttt.team_types_id = ".$request->input('team_types_id')."
    AND
    td.db_date = '".$request->input('match_day')."'
    GROUP BY f.id, tf.time_frames_id
    HAVING count_matches < 1
    ORDER BY f.field_name, tf2.time_frame_start


    ";


    $fields = DB::select($query);
    return $fields;
  }

  public function getFields(Request $request){
    $query = "SELECT
    f.field_name,
    f.id as fields_id,
    f.field_cost,
    (SELECT company_address FROM companies c WHERE c.id = f.companies_id) as field_address,
    tf.time_frames_id,
    tf2.time_frame_label,
    m.match_label
    FROM fields f
    LEFT JOIN fields_team_types_table fttt ON f.id = fttt.fields_id
    LEFT JOIN tournaments_fields tf ON f.id = tf.fields_id
    LEFT JOIN time_dimension td ON td.day_id = tf.days_id
    LEFT JOIN time_frames tf2 ON tf2.id = tf.time_frames_id
    LEFT JOIN matches m ON m.fields_id = f.id AND m.time_frames_id = tf2.id AND match_day = '".$request->input('match_day')."'
    WHERE
    fttt.team_types_id = ".$request->input('team_types_id')."
    AND
    td.db_date = '".$request->input('match_day')."'
    GROUP BY f.id, tf.time_frames_id
    ORDER BY f.field_name, tf2.time_frame_start


    ";


    $fields = DB::select($query);
    return $fields;
  }
}
