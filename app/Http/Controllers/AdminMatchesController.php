<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use PDF;

class AdminMatchesController extends \crocodicstudio\crudbooster\controllers\CBController {

	public function cbInit() {

		list($PARAM['admin'],$PARAM['method'],$PARAM['action'],$PARAM['id']) = explode('/',str_replace(url('/').'/', '', url()->current()));

			# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "tournaments_id";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = true;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = false;
		$this->button_export = false;
		$this->table = "matches";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"Squadra in casa","name"=>"teams_id_host","join"=>"teams,team_name"];
		$this->col[] = ["label"=>"Squadra ospite","name"=>"teams_id_guest","join"=>"teams,team_name"];
		$this->col[] = ["label"=>"Torneo","name"=>"
		(
		SELECT t.tournament_name
		FROM tournaments t
		LEFT JOIN tournament_effective_rounds ter ON t.id = ter.tournaments_id WHERE ter.id = matches.tournament_effective_rounds_id
	) as tournament_name"];
	$this->col[] = ["label"=>"Girone","name"=>"tournament_effective_rounds_id","join"=>"tournament_effective_rounds,round_label"];
	$this->col[] = ["label"=>"Giorno","name"=>"match_day"];
	$this->col[] = ["label"=>"Orario","name"=>"time_frames_id","join"=>"time_frames,time_frame_label"];
	$this->col[] = ["label"=>"Campo","name"=>"fields_id","join"=>"fields,field_name"];
	$this->col[] = ["label"=>"Arbitro","name"=>"referees_id","join"=>"referees,referee_full_name"];
	$this->col[] = ["label"=>"Delegate","name"=>"delegates_id","join"=>"delegates,delegate_full_name"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
	$this->form = [];
	$this->form[] = ['label'=>'Torneo/Girone','name'=>'tournament_effective_rounds_id','type'=>'select','datatable'=>'tournament_effective_rounds,round_label'];
	$this->form[] = ['label'=>'Squadra in casa','name'=>'teams_id_host','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'teams,team_name'];
	$this->form[] = ['label'=>'Squadra ospite','name'=>'teams_id_guest','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'teams,team_name'];
	$this->form[] = ['label'=>'Giorno','name'=>'match_day','type'=>'date','width'=>'col-sm-10'];
	$this->form[] = ['label'=>'Orario','name'=>'time_frames_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'time_frames,time_frame_label'];
	$this->form[] = ['label'=>'Campo','name'=>'fields_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'fields,field_name'];
	$this->form[] = ['label'=>'Stato della partita','name'=>'match_status','type'=>'select2','width'=>'col-sm-10','dataenum'=>'-;Giocata;Rimandata;Cancellata;Sospesa;Finita ai rigoi;Vinta a tavolino'];
	$this->form[] = ['label'=>'Arbitro','name'=>'referees_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'referees,referee_full_name'];
	$this->form[] = ['label'=>'Delegato','name'=>'delegates_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'delegates,delegate_full_name'];

			### MATCH EVENTS ###

	if($PARAM['id']>0){
		$match_events_fields[] = [
			'label'=>'Squadra',
			'name'=>'teams_id',
			'type'=>'datamodal',
			'datamodal_table'=>'teams',
			'datamodal_where'=>'
			id IN (
			SELECT teams_id_host id FROM matches WHERE id = '.$PARAM['id'].'
			UNION
			SELECT teams_id_guest id FROM matches WHERE id = '.$PARAM['id'].'
		)',
		'datamodal_columns'=>'team_name',
		'datamodal_size'=>'small'
	];

	$match_events_fields[] = [
		'label'=>'Giocatore',
		'name'=>'players_id','type'=>'datamodal',
		'datamodal_table'=>'players',
		'datamodal_where'=>'
		id IN (
		SELECT players_id FROM teams_players WHERE teams_id IN (SELECT teams_id_host id FROM matches WHERE id = '.$PARAM['id'].'
		UNION
		SELECT teams_id_guest id FROM matches WHERE id = '.$PARAM['id'].')
	)',
	'datamodal_columns'=>'player_full_name,player_date_of_birth,player_licensee_number',
	'datamodal_columns_alias'=>'Giocatore,Data di nascita,Cartellino',
	'datamodal_size'=>'small'
];



$match_events_fields[] = ['label'=>'Minuto','name'=>'match_event_minute','type'=>'number'];
$match_events_fields[] = ['label'=>'Evento','name'=>'match_event_types_id','type'=>'select','datatable'=>'match_event_types,event_type_name'];
$match_events_fields[] = ['label'=>'Note','name'=>'match_event_notes','type'=>'textarea'];

$this->form[] = ['label'=>'Eventi','name'=>'match_events','type'=>'child','columns'=>$match_events_fields,'table'=>'match_events','foreign_key'=>'matches_id'];
}

			### MATCH EVENTS ###

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Torneo','name'=>'tournament_effective_rounds_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'tournament_effective_rounds,round_label'];
			//$this->form[] = ['label'=>'Squadra in casa','name'=>'teams_id_host','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'teams,team_name'];
			//$this->form[] = ['label'=>'Squadra ospite','name'=>'teams_id_guest','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'teams,team_name'];
			//$this->form[] = ['label'=>'Giorno','name'=>'match_day','type'=>'date','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Orario','name'=>'time_frames_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'time_frames,time_frame_label'];
			//$this->form[] = ['label'=>'Campo','name'=>'fields_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'fields,field_name'];
			//$this->form[] = ['label'=>'Stato della partita','name'=>'match_status','type'=>'select2','width'=>'col-sm-10','dataenum'=>'Da giocare;Giocata;Rimandata;Cancellata;Sospesa'];
			//$this->form[] = ['label'=>'Arbitro','name'=>'referees_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'referees,referee_full_name'];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
	        $this->addaction = array();
	        $this->addaction[] = ['label'=>'Stampa in Casa','url'=>CRUDBooster::mainpath('print-match-host/[id]'),'icon'=>'fa fa-print','color'=>'primary'];
	        $this->addaction[] = ['label'=>'Stampa Ospite','url'=>CRUDBooster::mainpath('print-match-guest/[id]'),'icon'=>'fa fa-print','color'=>'primary'];



	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();

	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();
	        $this->index_button[] = ["label"=>"Stampa partite","icon"=>"fa fa-print","url"=>CRUDBooster::mainpath('print-matches-list').'/?'.http_build_query($_GET)];
	        $this->index_button[] = ["label"=>"Imposta risultati","icon"=>"fa fa-list","url"=>CRUDBooster::mainpath('set-matches-results').'/?'.http_build_query($_GET)];
					$this->index_button[] = ["label"=>"Pulisci partite","icon"=>"fa fa-eraser","url"=>CRUDBooster::mainpath('clean-matches')];


	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        ob_start();
	        /*echo '<pre>';
	        print_r($_GET);
	        echo '</pre>';*/
	        $tournaments= DB::table('tournaments')->get();
	        $fields = DB::table('fields')->get();
	        $_GET['filter_column']['matches.match_day']['value'][0] = ($_GET['filter_column']['matches.match_day']['value'][0]) ? : date('Y-m-d');
	        $_GET['filter_column']['matches.match_day']['value'][1] = ($_GET['filter_column']['matches.match_day']['value'][1]) ? : date('Y-m-d',strtotime('+7days'));
	        ?>
	        <form class="form form-inline" method="/admin/matches">
	        	<input type="hidden" name="filter_column[matches.match_day][type]" value="between">
	        	<input type="hidden" name="filter_column[fields.field_name][type]" value="like">
	        	<div class="form-group">
	        		<label value="">Torneo:</label>
	        		<select name="extra_filters[tournaments_id][value]" id="" class="form-control">
	        			<option>Scegli...</option>
	        			<?php foreach($tournaments as $tournament){?>
	        				<option <?php echo ($_GET['extra_filters']['tournaments_id']['value']==$tournament->id) ? 'selected' : ''?> value="<?php echo $tournament->id?>"><?php echo $tournament->tournament_name?></option>
	        			<?php }?>
	        		</select>
	        	</div>
	        	<div class="form-group">
	        		<label>Campo:</label>
	        		<!--filter_column[fields.field_name][value]-->
	        		<select name="filter_column[fields.field_name][value]" id="" class="form-control">
		        		<option value="">Scegli...</option>
		        		<?php foreach($fields as $field){?>
		        				<option <?php echo ($_GET['filter_column']['fields.field_name']['value']==$field->field_name) ? 'selected' : ''?> value="<?php echo $field->field_name?>"><?php echo $field->field_name?></option>
		        		<?php }?>
	        		</select>
	        	</div>
	        	<div class="form-group">
	        		<label>Da:</label>
	        		<input value="<?php echo($_GET['filter_column']['matches.match_day']['value'][0]) ?>" name="filter_column[matches.match_day][value][0]" type="text" class="form-control datepicker">
	        	</div>
	        	<div class="form-group">
	        		<label>A:</label>
	        		<input value="<?php echo($_GET['filter_column']['matches.match_day']['value'][1]) ?>" name="filter_column[matches.match_day][value][1]" type="text" class="form-control datepicker">
	        	</div>
	        	<button style="margin-top:24px" type="submit" class="btn btn-sm btn-success">Filtra</button>
	        </form>
	        <?php
	        $cane = ob_get_clean();
	        $this->pre_index_html = $cane;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }

			public function getCleanMatches(){
				$query = "DELETE FROM matches WHERE match_label != 'Campo prenotato' AND (teams_id_host = 0 OR teams_id_guest = 0)";

				DB::statement($query);

				CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"Partite pulite","success");
			}

	    public function print_rr($array){
	    	echo '<pre>';
	    	print_r($array);
	    	echo '</pre>';
	    }

	    public function bulkResultsUpdater(){
	    	$matches = DB::table('matches')->get();
	    	foreach($matches as $match){
	    		echo $match->match_label.'<br/>';;
	    		$this->calculateMatchResult2($match->id,1);
	    		echo '<br/><br/>';
	    	}
	    }

	    public function getSetMatchTeamScore(){
	    	$g = $_GET;
	    	$sql = DB::table('matches')
	    	->where('id',$g['matches_id'])
	    	->update([
	    		'teams_id_'.$g['teams_type'].'_goals'=>$g['goals'],
	    		'result_set'=>1
	    	]);
	    	$this->assignMatchResult($g['matches_id']);
	    	$this->calculateMatchResult2($g['matches_id']);
	    	exit;
	    }

	    public function getEditMatchEventByType(){
	    	$g = $_GET;

	    	DB::table('match_events')
	    	->where('teams_id',$g['teams_id'])
	    	->where('matches_id',$g['matches_id'])
	    	->where('match_event_types_id',$g['match_event_types_id'])
	    	->delete();

	    	if(count($g['players_id'])){
	    		foreach($g['players_id'] as $players_id => $events){
	    			if($events>0 && $players_id > 0){
	    				for($i=0; $i<$events; $i++){
	    					DB::table('match_events')
			    			->insert([
			    				[
			    					'teams_id'=>$g['teams_id'],
			    					'matches_id'=>$g['matches_id'],
			    					'match_event_types_id'=>$g['match_event_types_id'],
			    					'players_id'=>$players_id
			    				]
			    			]);
	    				}
	    			}

	    		}
	    	}
	    	if($g['match_event_types_id']==1){
	    		//$this->calculateMatchResult2($g['matches_id']);
	    	}

	    }

	    public function getSetMatchesResults($data=[]){
	    	$filters = $_GET['filter_column'];
	    	if(isset($filters['matches.match_day']['value'][0]) && isset($filters['matches.match_day']['value'][1]) && $filters['matches.match_day']['type'] =='between'){

	    		$matches_ = DB::table('matches')
	    		->select(
	    			'matches.id as matches_id',
	    			'matches.*',
	    			'fields.*',
	    			'time_frames.*',
	    			'tournament_effective_rounds.*',
	    			'tournaments.*',
	    			DB::raw('(SELECT team_name FROM teams WHERE teams.id = matches.teams_id_host) as team_host_name'),
	    			DB::raw('(SELECT team_name FROM teams WHERE teams.id = matches.teams_id_guest) as team_guest_name')
	    		)
	    		->orderBy('match_day')
	    		->leftJoin('fields','matches.fields_id','=','fields.id')
	    		->leftJoin('time_frames','matches.time_frames_id','=','time_frames.id')
	    		->leftJoin('tournament_effective_rounds','matches.tournament_effective_rounds_id','=','tournament_effective_rounds.id')
	    		->leftJoin('tournaments','tournament_effective_rounds.tournaments_id','=','tournaments.id')
	    		->whereBetween('matches.match_day',[$filters['matches.match_day']['value'][0],$filters['matches.match_day']['value'][1]])
	    		->get();

	    		$matches_ids = [];
	    		if(count($matches_)){
	    			foreach($matches_ as $index => $match){
	    				$matches_ids[] = $match->matches_id;
	    				$matches[$index] = $match;
	    				$matches[$index]->team_host_players =
	    				DB::table('players')
	    				->leftJoin('teams_players','player.id','=','teams_players.player_id')
	    				->where('teams_players.teams_id',$match->teams_id_host);
	    				########### load guest team players
	    				$team_guest_players = [];
	    				$team_guest_players_ =
	    				DB::table('players')
	    				->select('players.*')
	    				->leftJoin('teams_players','players.id','=','teams_players.players_id')
	    				->where('teams_players.teams_id',$match->teams_id_guest)
	    				->get();
	    				if(count($team_guest_players_)){
	    					foreach($team_guest_players_ as $player){
	    						$team_guest_players[$player->id] = $player;
	    					}
	    				}

	    				$matches[$index]->team_guest_players = $team_guest_players;
	    				########### load host team players
	    				$team_host_players = [];
	    				$team_host_players_ =
	    				DB::table('players')
	    				->select('players.*')
	    				->leftJoin('teams_players','players.id','=','teams_players.players_id')
	    				->where('teams_players.teams_id',$match->teams_id_host)
	    				->get();
	    				if(count($team_host_players_)){
	    					foreach($team_host_players_ as $player){
	    						$team_host_players[$player->id] = $player;
	    					}
	    				}
	    				$matches[$index]->team_host_players = $team_host_players;
	    			}
	    		}

	    		$events_ = DB::table('match_events')->whereIn('matches_id',$matches_ids)->get();
	    		$events = [];

	    		$event_types =  DB::table('match_event_types')->orderBY('id','asc')->get();

	    		$c = 0;

	    		if(count($events_)>0){
	    			foreach($events_ as $event){
	    				$events[$event->matches_id][$event->teams_id][$event->match_event_types_id][$event->players_id]['players_id'] = $event->players_id;
	    				$events[$event->matches_id][$event->teams_id][$event->match_event_types_id][$event->players_id]['events']++;
	    			}
	    		}

	    		#$this->print_rr($events);

	    		$data['event_types_asc'] = $event_types;
	    		$data['event_types_desc'] = DB::table('match_event_types')->orderBY('id','desc')->get();
	    		$data['events'] = $events;#events organized by match id
	    		$data['matches'] = $matches;
	    		$data['event_colors'] = [2=>'yellow',3=>'red'];

	    		$this->cbView('matches_results',$data);
	    	}else{
	    		CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"Devi impostare il periodo per impostare i risultati delle partite.","warning");
	    	}
	    }

	    public function getPrintMatchesList($data=[]){

	    	$days_of_the_week = ['Domenica','Lunedì','Martedì','Mercoledì','Giovedì','Venerdì','Sabato'];

	    	$filters = $_GET['filter_column'];
	    	if(isset($filters['matches.match_day']['value'][0]) && isset($filters['matches.match_day']['value'][1]) && $filters['matches.match_day']['type'] =='between'){
	    		$matches = DB::table('matches')
	    		->orderBy('match_day','ASC')
	    		->leftJoin('fields','matches.fields_id','=','fields.id')
	    		->leftJoin('time_frames','matches.time_frames_id','=','time_frames.id')
	    		->leftJoin('tournament_effective_rounds','matches.tournament_effective_rounds_id','=','tournament_effective_rounds.id')
	    		->leftJoin('tournaments','tournament_effective_rounds.tournaments_id','=','tournaments.id')

		    	->leftJoin('referees','referees.id','=','matches.referees_id')
				->leftJoin('delegates','delegates.id','=','matches.delegates_id')
				->where('match_label','!=','Campo prenotato')
	    		->whereBetween('matches.match_day',[$filters['matches.match_day']['value'][0],$filters['matches.match_day']['value'][1]])
	    		->orderBy('field_name','ASC')
	    		->orderBy('time_frames_id','ASC');


	    		if(!empty($filters['fields.field_name']['value'])){
	    			$matches -> where('fields.field_name',$filters['fields.field_name']['value']);
	    		}

	    		$matches = $matches->get();

	    		$matches_indexed = [];
	    		$fields = [];
	    		$days = [];
	    		if(count($matches)){
	    			foreach($matches as $match){
	    				$day = $days_of_the_week[date('w',strtotime($match->match_day))].' '.date('d/m/Y',strtotime($match->match_day));
	    				$matches_indexed[$day][$match->field_name][] = $match;
	    				$days[] = $match->match_day;
	    			}
	    		}
	    		$days = array_unique($days);

	    		$pdf = PDF::loadView('mail_test_2',[
	    			'match_groups' => $matches_indexed,
	    			'matches_from' => $days_of_the_week[date('w',strtotime($filters['matches.match_day']['value'][0]))].' '.date('d/m/Y',strtotime($filters['matches.match_day']['value'][0])),
	    			'matches_to' => $days_of_the_week[date('w',strtotime($filters['matches.match_day']['value'][1]))].' '.date('d/m/Y',strtotime($filters['matches.match_day']['value'][1])),
	    		]);

	    		return $pdf->setPaper('A4')->download('partite dal '.$filters['matches.match_day']['value'][0].' al '.$filters['matches.match_day']['value'][1].'.pdf');
	    	}else{
	    		CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"Devi impostare il periodo per la stampa.","warning");
	    	}
	    }



	    public function getPrintMatchHost($id) {
	    	$match = DB::table('matches')
	    	->leftJoin('fields','matches.fields_id','=','fields.id')
	    	->leftJoin('time_frames','matches.time_frames_id','=','time_frames.id')
	    	->leftJoin('referees','referees.id','=','matches.referees_id')
	    	->leftJoin('delegates','delegates.id','=','matches.delegates_id')
	    	->where('matches.id',$id)->get()[0];
	    	$tournament = DB::table('tournaments')
	    	->leftJoin('tournament_effective_rounds','tournaments.id','=','tournament_effective_rounds.tournaments_id')
	    	->where('tournament_effective_rounds.id',$match->tournament_effective_rounds_id)
	    	->get()[0];
	    	$team_host = DB::table('teams')->where('id',$match->teams_id_host)->get()[0];
	    	$team_host_players = DB::table('players')->leftJoin('teams_players','teams_players.players_id','=','players.id')->where('teams_players.teams_id',$team_host->id)->orderBy('player_last_name')->get();

	    	/*
	    	return view('mail_test_1',[
	    		'team_name' => $team_host->team_name,
	    		'match_label' => $match->match_label,
	    		'tournament_name' => $tournament->tournament_name,
	    		'match_day' => $match->match_day,
	    		'field_name'=> $match->field_name,
	    		'round_label'=> $tournament->round_label,
	    		'time_frame_start' => $match->time_frame_start,
	    		'players' => $team_host_players
	    	]);
	    	exit;*/
	    	$pdf = PDF::loadView('mail_test_1',[
	    		'team_name' => $team_host->team_name,
	    		'match_label' => $match->match_label,
	    		'tournament_name' => $tournament->tournament_name,
	    		'match_day' => date('d-m-Y',strtotime($match->match_day)),
	    		'field_name'=> $match->field_name,
	    		'round_label'=> $tournament->round_label,
	    		'time_frame_start' => date('H:i',strtotime($match->time_frame_start)),
	    		'players' => $team_host_players,
	    		'referee' => $match->referee_full_name,
	    		'delegate' => $match->delegate_full_name,
	    	]);
	    	return $pdf->setPaper('A4')->download($match->match_label.' '.$tournament->round_label.' '.$team_host->team_name.'.pdf');
	    }

	    public function getPrintMatchGuest($id) {
	    	$match = DB::table('matches')
	    	->leftJoin('fields','matches.fields_id','=','fields.id')
	    	->leftJoin('time_frames','matches.time_frames_id','=','time_frames.id')
	    	->leftJoin('referees','referees.id','=','matches.referees_id')
	    	->leftJoin('delegates','delegates.id','=','matches.delegates_id')
	    	->where('matches.id',$id)->first();

	    	$tournament = DB::table('tournaments')
	    	->leftJoin('tournament_effective_rounds','tournaments.id','=','tournament_effective_rounds.tournaments_id')
	    	->where('tournament_effective_rounds.id',$match->tournament_effective_rounds_id)
	    	->first();
	    	$team_guest = DB::table('teams')->where('id',$match->teams_id_guest)->first();
	    	$team_guest_players = DB::table('players')->leftJoin('teams_players','teams_players.players_id','=','players.id')->where('teams_players.teams_id',$team_guest->id)->orderBy('player_last_name')->get();
	    	$data = [
	    		'team_name' => $team_guest->team_name,
	    		'match_label' => $match->match_label,
	    		'tournament_name' => $tournament->tournament_name,
	    		'match_day' =>  date('d-m-Y',strtotime($match->match_day)),
	    		'field_name'=> $match->field_name,
	    		'round_label'=> $tournament->round_label,
	    		'time_frame_start' => date('H:i',strtotime($match->time_frame_start)),
	    		'players' => $team_guest_players,
	    		'referee' => $match->referee_full_name,
	    		'delegate' => $match->delegate_full_name,
	    	];

	    	$pdf = PDF::loadView('mail_test_1',$data);
	    	return $pdf->setPaper('A4')->download($match->match_label.' '.$tournament->round_label.' '.$team_guest->team_name.'.pdf');
	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	    	switch($button_name){

	    	}
	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {

			$query->where('teams_id_host','!=',0);
			$query->where('match_label','!=','Campo prenotato');

	    	if((int)$_GET['extra_filters']['tournaments_id']['value']>0){
	    		#$this->print_rr($_GET);
	    		$query->whereIn('matches.tournament_effective_rounds_id',function($query1){
	    			$query1->select('id')->from('tournament_effective_rounds')->where('tournaments_id',$_GET['extra_filters']['tournaments_id']['value']);
	    		});
	    	}
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
	    	//Your code here
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        //Your code here
	    	$team_host = (array)DB::table('teams')->where('id',$postdata['teams_id_host'])->get()[0];
	    	$team_guest = (array)DB::table('teams')->where('id',$postdata['teams_id_guest'])->get()[0];
	    	$postdata['match_label'] = $team_host['team_name'].' - '.$team_guest['team_name'];

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */
	    public function hook_after_add($id) {
	        //Your code here
//Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	    	$team_host = (array)DB::table('teams')->where('id',$postdata['teams_id_host'])->get()[0];
	    	$team_guest = (array)DB::table('teams')->where('id',$postdata['teams_id_guest'])->get()[0];
	    	$postdata['match_label'] = $team_host['team_name'].' - '.$team_guest['team_name'];

	    	DB::table('matches')->where('id',$id)->update(['teams_id_host_goals'=>0,'teams_id_guest_goals'=>0,'teams_id_winner'=>NULL]);
	    	DB::table('matches_teams')->where('matches_id',$id)->delete();

	    	DB::table('matches_teams')->insert([
	    		[
	    			'matches_id'=>$id,
	    			'teams_id' => $postdata['teams_id_guest'],
	    			'matches_teams_mode' => 'guest'
	    		],
	    		[
	    			'matches_id'=>$id,
	    			'teams_id' => $postdata['teams_id_host'],
	    			'matches_teams_mode' => 'host'
	    		]
	    	]);

	    }







	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */

	    public function assignMatchResult($id){
	    	#refresh match value
	    		$match = DB::table('matches')->where('id',$id)->first();

	    		if($match->teams_id_host_goals > $match->teams_id_guest_goals){
	    			DB::table('matches')->where('id',$match->id)->update(['teams_id_winner'=>$match->teams_id_host]);
	    			DB::table('matches_teams')
	    			->where('matches_id',$id)
	    			->where('teams_id', $match->teams_id_host)
	    			->update(['points'=>3,'result'=>'win']);
	    			DB::table('matches')->where('id',$match->id)->update(['teams_id_winner'=>$match->teams_id_guest]);
	    			DB::table('matches_teams')
	    			->where('matches_id',$id)
	    			->where('teams_id', $match->teams_id_guest)
	    			->update(['points'=>0,'result'=>'lose']);
	    		}
	    		else if($match->teams_id_guest_goals > $match->teams_id_host_goals){
	    			DB::table('matches')->where('id',$match->id)->update(['teams_id_winner'=>$match->teams_id_guest]);
	    			DB::table('matches_teams')
	    			->where('matches_id',$id)
	    			->where('teams_id', $match->teams_id_guest)
	    			->update(['points'=>3,'result'=>'win']);
	    			DB::table('matches')->where('id',$match->id)->update(['teams_id_winner'=>$match->teams_id_host]);
	    			DB::table('matches_teams')
	    			->where('matches_id',$id)
	    			->where('teams_id', $match->teams_id_host)
	    			->update(['points'=>0,'result'=>'lose']);
	    		}
	    		else if($match->teams_id_guest_goals == $match->teams_id_host_goals){
	    			DB::table('matches')->where('id',$match->id)->update(['teams_id_winner'=>NULL]);
	    			DB::table('matches_teams')
	    			->where('matches_id',$id)
	    			->where('teams_id', $match->teams_id_host)
	    			->update(['points'=>1,'result'=>'draw']);
	    			DB::table('matches_teams')
	    			->where('matches_id',$id)
	    			->where('teams_id', $match->teams_id_guest)
	    			->update(['points'=>1,'result'=>'draw']);
	    		}
	    }

	    public function calculateMatchResult2($id,$debug=0){

	    	$match = DB::table('matches')->where('id',$id)->first();
	    	/*DB::table('matches_teams')->where('matches_id',$id)->delete();
	    	DB::table('matches_teams')->insert([
	    			[
	    				'matches_id'=>$id,
	    				'teams_id' => $match->teams_id_guest,
	    				'matches_teams_mode' => 'guest'
	    			],
	    			[
	    				'matches_id'=>$id,
	    				'teams_id' => $match->teams_id_host,
	    				'matches_teams_mode' => 'host'
	    			]
	    		]);*/

	    	if($debug==1){
	    		echo 'teams_id '.$match->teams_id_host.' | in '.$match->teams_id_guest_goals.' | out '.$match->teams_id_host_goals.'<br/>';
	    		echo 'teams_id '.$match->teams_id_guest.' | in '.$match->teams_id_host_goals.' | out '.$match->teams_id_guest_goals.'<br/><br/>';
	    	}

	    	#DB::table('matches')->where('id',$id)->update(['teams_id_guest_goals'=>0,'teams_id_host_goals'=>0]);

	    	DB::table('matches_teams')
			->where('matches_id',$id)
			->where('teams_id', $match->teams_id_host)
			->update(['scores_in'=>$match->teams_id_guest_goals,'scores_out'=>$match->teams_id_host_goals]);

			DB::table('matches_teams')
			->where('matches_id',$id)
			->where('teams_id', $match->teams_id_guest)
			->update(['scores_in'=>$match->teams_id_host_goals,'scores_out'=>$match->teams_id_guest_goals]);

	    }

	    public function calculateMatchResult($id){



	    	$events = DB::table('match_events')->where('matches_id',$id)->get();

	    	if(count($events)){
	    		$match = DB::table('matches')->where('id',$id)->first();

	    		DB::table('matches_teams')->where('matches_id',$id)->delete();
	    		DB::table('matches_teams')->insert([
	    			[
	    				'matches_id'=>$id,
	    				'teams_id' => $match->teams_id_guest,
	    				'matches_teams_mode' => 'guest'
	    			],
	    			[
	    				'matches_id'=>$id,
	    				'teams_id' => $match->teams_id_host,
	    				'matches_teams_mode' => 'host'
	    			]
	    		]);

	    		DB::table('matches')->where('id',$id)->update(['teams_id_guest_goals'=>0,'teams_id_host_goals'=>0]);

	    		foreach($events as $event){

	    			switch($event->match_event_types_id){
	    				case 1:
	    				if($event->teams_id == $match->teams_id_host){

	    					#DB::table('matches')->where('id',$match->id)->increment('teams_id_host_goals');

	    					DB::table('matches_teams')
	    					->where('matches_id',$id)
	    					->where('teams_id', $match->teams_id_host)
	    					->increment('scores_out');

	    					DB::table('matches_teams')
	    					->where('matches_id',$id)
	    					->where('teams_id', $match->teams_id_guest)
	    					->increment('scores_in');
	    				}
	    				if($event->teams_id == $match->teams_id_guest){

	    					DB::table('matches')->where('id',$match->id)->increment('teams_id_guest_goals');

	    					DB::table('matches_teams')
	    					->where('matches_id',$id)
	    					->where('teams_id', $match->teams_id_host)
	    					->increment('scores_in');

	    					DB::table('matches_teams')
	    					->where('matches_id',$id)
	    					->where('teams_id', $match->teams_id_guest)
	    					->increment('scores_out');
	    				}
	    				break;
	    			}
	    		}

	    		$this->assignMatchResult($id);

	    	}
	    }

	    public function hook_after_edit($id) {
	        //Your code here
	    	$this->calculateMatchResult($id);
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

		}

		public function getMatchDetails($id){
			$events = DB::table('match_events')
			->select(
				'match_event_types.event_type_name',
				'match_event_types.id as event_type_id',
				'teams.team_name',
				'players.player_full_name'
				)
			->leftJoin('match_event_types','match_events.match_event_types_id','=','match_event_types.id')
			->leftJoin('players','match_events.players_id','=','players.id')
			->leftJoin('teams','match_events.teams_id','=','teams.id')
			->where('match_events.matches_id',$id)
			->orderBy('teams.team_name','asc')
			->orderBy('match_event_types.id','asc')
			->get();
			return $events;
		}



	    //By the way, you can still create your own method in here... :)


	}
