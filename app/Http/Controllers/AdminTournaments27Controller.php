<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;

class AdminTournaments27Controller extends \crocodicstudio\crudbooster\controllers\CBController {

	public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "tournament_name";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = true;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = false;
		$this->button_export = false;
		$this->table = "tournaments";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"Nome","name"=>"tournament_name"];
		$this->col[] = ["label"=>"Gioco","name"=>"team_types_id","join"=>"team_types,team_type_name"];
		$this->col[] = ["label"=>"Tipo di torneo","name"=>"tournament_types_id","join"=>"tournament_types,tournament_type_label"];
		$this->col[] = ["label"=>"Simmetrico","name"=>"tournament_symmetric"];
		$this->col[] = ["label"=>"Stato","name"=>"tournament_status"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
		$this->form[] = ['label'=>'Nome','name'=>'tournament_name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Gioco','name'=>'team_types_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'team_types,team_type_name'];
		$this->form[] = ['label'=>'Tipo di torneo','name'=>'tournament_types_id','type'=>'select2','validation'=>'required','width'=>'col-sm-9','datatable'=>'tournament_types,tournament_type_label'];
		$this->form[] = ['label'=>'Simmetrico','name'=>'tournament_symmetric','type'=>'radio','width'=>'col-sm-10','dataenum'=>'1|Simmetrico;0|Non simmetrico'];
		$this->form[] = ['label'=>'Giorno di inizio','name'=>'begin_day','type'=>'date','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Status','name'=>'tournament_status','type'=>'radio','width'=>'col-sm-10',"dataenum"=>"1|Attivo;0|Non attivo"];
		//$this->form[] = ['label'=>'Fase finale','name'=>'final_phase','type'=>'radio','width'=>'col-sm-10',"dataenum"=>"1|Attivo;0|Non attivo"];



		$columns_fields[] = ['label'=>'Campo','name'=>'fields_id','type'=>'datamodal','datamodal_table'=>'fields','datamodal_columns'=>'field_name','datamodal_size'=>'small','required'=>true];
		$columns_fields[] = ['label'=>'Giorno','name'=>'days_id','type'=>'datamodal','datamodal_table'=>'days','datamodal_columns'=>'day_name','datamodal_size'=>'small','required'=>true];
		$columns_fields[] = ['label'=>'Orario','name'=>'time_frames_id','type'=>'datamodal','datamodal_table'=>'time_frames','datamodal_columns'=>'time_frame_label','datamodal_size'=>'small','required'=>true];

		$this->form[] = ['label'=>'Prenotazioni campi','name'=>'tournaments_fields','type'=>'child','columns'=>$columns_fields,'table'=>'tournaments_fields','foreign_key'=>'tournaments_id'];


		$columns_rounds[] = ['label'=>'Categoria','name'=>'team_categories_id','type'=>'datamodal','datamodal_table'=>'team_categories','datamodal_columns'=>'team_category_name','datamodal_size'=>'small','required'=>true];
		$columns_rounds[] = ['label'=>'Tipo','name'=>'team_kinds_id','type'=>'datamodal','datamodal_table'=>'team_kinds','datamodal_columns'=>'team_kind_name','datamodal_size'=>'small','required'=>true];
		$columns_rounds[] = ['label'=>'Numero','name'=>'rounds_number','type'=>'number','required'=>true,'min'=>1,'max'=>10];
		$this->form[] = ['label'=>'Gironi','name'=>'tournament_rounds','type'=>'child','columns'=>$columns_rounds,'table'=>'tournament_rounds','foreign_key'=>'tournaments_id'];


		$columns_teams[] = ['label'=>'Squadra','name'=>'teams_id','type'=>'datamodal','datamodal_table'=>'teams','datamodal_columns'=>'team_name','datamodal_size'=>'small','required'=>true];
		$columns_teams[] = ['label'=>'Campo preferito','name'=>'fields_id','type'=>'datamodal','datamodal_table'=>'fields','datamodal_columns'=>'field_name','datamodal_size'=>'small','required'=>true];
		$columns_teams[] = ['label'=>'Giorno preferito','name'=>'days_id','type'=>'datamodal','datamodal_table'=>'days','datamodal_columns'=>'day_name','datamodal_size'=>'small','required'=>true];
		$columns_teams[] = ['label'=>'Orario preferito','name'=>'time_frames_id','type'=>'datamodal','datamodal_table'=>'time_frames','datamodal_columns'=>'time_frame_label','datamodal_size'=>'small','required'=>true];
		$this->form[] = ['label'=>'Squadre','name'=>'tournaments_teams_preferences','type'=>'child','columns'=>$columns_teams,'table'=>'tournaments_teams_preferences','foreign_key'=>'tournaments_id'];

		$columns_categories_costs[] = ['label'=>'Categoria','name'=>'team_categories_id','type'=>'datamodal','datamodal_table'=>'team_categories','datamodal_columns'=>'team_category_name','datamodal_size'=>'small','required'=>true];
		$columns_categories_costs[] = ['label'=>'Costo','name'=>'tournament_category_cost_value','type'=>'number','required'=>true];
		$this->form[] = ['label'=>'Costi per categoria','name'=>'tournament_category_costs','type'=>'child','columns'=>$columns_categories_costs,'table'=>'tournament_category_costs','foreign_key'=>'tournaments_id'];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Nome','name'=>'tournament_name','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Gioco','name'=>'team_types_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'team_types,team_type_name'];
			//$this->form[] = ['label'=>'Tipo di torneo','name'=>'tournament_types_id','type'=>'select2','validation'=>'required','width'=>'col-sm-9','datatable'=>'tournament_types,tournament_type_label'];
			//$this->form[] = ['label'=>'Simmetrico','name'=>'tournament_symmetric','type'=>'radio','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Campi','name'=>'fields_id','type'=>'select2','width'=>'col-sm-9','datatable'=>'fields,field_name'];
			//$this->form[] = ['label'=>'Giorni','name'=>'days_id','type'=>'select2','width'=>'col-sm-9','datatable'=>'days,day_name','relationship_table'=>'tournaments_days'];
			//$this->form[] = ['label'=>'Orari','name'=>'time_frames_id','type'=>'select2','width'=>'col-sm-9','datatable'=>'time_frames,time_frame_label','relationship_table'=>'tournaments_time_frames'];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
	        $this->addaction = array();
	        $this->addaction[] = ['label'=>'Dettaglio partite','url'=>CRUDBooster::mainpath('matches/[id]'),'icon'=>'fa fa-eye','color'=>'primary'];
	        $this->addaction[] = ['label'=>'Genera partite','url'=>CRUDBooster::mainpath('matches-generator/[id]'),'icon'=>'fa fa-warning','color'=>'warning', 'confirmation' => true];
	        $this->addaction[] = ['label'=>'Associa squadre','url'=>CRUDBooster::mainpath('associate-tournament-teams/[id]'),'icon'=>'fa fa-warning','color'=>'warning', 'confirmation' => true];;
	        $this->addaction[] = ['label'=>'Associa campi','url'=>CRUDBooster::mainpath('associate-tournament-fields/[id]'),'icon'=>'fa fa-warning','color'=>'warning', 'confirmation' => true];;
					

	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert        = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }



	    public function print_rr($array){
	    	echo '<pre>';
	    	print_r($array);
	    	echo '</pre>';
	    }

	    public function getSetMatchSlot($matches_id='',$fields_id='',$day='',$time_frames_id='',$tournaments_id=''){
	    	if($matches_id!=''){
	    		DB::table('matches')
	    		->where('id',$matches_id)
	    		->update([
	    			'fields_id' => $fields_id,
	    			'match_day' => $day,
	    			'time_frames_id'=>$time_frames_id
	    		]);

	    		$slots = DB::select('SELECT fields_id, match_day, time_frames_id, COUNT(m.id) as count_matches
	    			FROM matches m
	    			GROUP BY m.fields_id,m.match_day, m.time_frames_id
	    			HAVING count_matches > 1');

	    		echo json_encode($slots);
	    	}
	    }

	    public function getMatches($id){
	    	$data['page_title'] = "Dettaglio partite";
	    	$days = [];


	    	$days_of_the_week['it-it'] = ['long'=>[],'short'=>[1=>'Lun',2=>'Mar',3=>'Mer',4=>'Gio',5=>'Ven',6=>'Sab',7=>'Dom']];

	    	$first_match = DB::table('matches')
	    	->select('matches.*')
	    	->join('tournament_effective_rounds','tournament_effective_rounds.id','=','matches.tournament_effective_rounds_id')
	    	->where('tournament_effective_rounds.tournaments_id',$id)
	    	->orderBy('match_day', 'asc')
	    	->first();

	    	$last_match = DB::table('matches')
	    	->select('matches.*')
	    	->join('tournament_effective_rounds','tournament_effective_rounds.id','=','matches.tournament_effective_rounds_id')
	    	->where('tournament_effective_rounds.tournaments_id',$id)
	    	->orderBy('match_day', 'desc')
	    	->first();
	    	$date_start = date_create($first_match->match_day);
	    	$date_end = date_create($last_match->match_day);
	    	$diff  	= date_diff( $date_start, $date_end );

	    	$days_num = $diff->days+15;

	    	for($i=0; $i<$days_num; $i++){
	    		$days[] = date('Y-m-d',strtotime('+'.$i.'days'));
	    		$daysN[] = date('Y-m-d',strtotime('+'.$i.'days')).' '.$days_of_the_week['it-it']['short'][date('N',strtotime('+'.$i.'days'))];
	    		$daysNa[] = $days_of_the_week['it-it']['short'][date('N',strtotime('+'.$i.'days'))];

	    	}

	    	$matches_ = DB::table('matches')
	    	->whereBetween('match_day',[$days[0],$days[$days_num-1]])->whereNotNull('fields_id')->get();

	    	$matches = [];
	    	$matches_per_slot = [];

	    	if(count($matches_)){
	    		foreach ($matches_ as $key => $value) {
	    			$matches[$value->tournament_effective_rounds_id][$value->fields_id][$value->match_day][$value->time_frames_id][] = $value;
	    			$matches_per_slot[$value->fields_id][$value->match_day][$value->time_frames_id] ++;
	    		}
	    	}

	    	$rounds =
	    	DB::table('tournament_effective_rounds')
	    	->where('tournaments_id',$id)
	    	->get();


	    	$all_fields =
	    	DB::table('fields')
	    	->select('field_name','fields_id')
	    	->join('tournaments_fields','fields.id','=','tournaments_fields.fields_id')
	    	->groupBy('fields_id')
	    	->where('tournaments_id',$id)
	    	->get();







	    	$fields =
	    	DB::table('fields')
	    	->select('fields.*','tournaments_fields.*','time_frames.*','days.*','tournaments_fields.id as tournaments_fields_id')
	    	->join('tournaments_fields','fields.id','=','tournaments_fields.fields_id')
	    	->join('time_frames','time_frames.id','=','tournaments_fields.time_frames_id')
	    	->join('days','days.id','=','tournaments_fields.days_id')
	    	->where('tournaments_fields.tournaments_id',$id)
	    	->get();
	    	$fields_ = [];
	    	if(count($fields)){
	    		foreach ($fields as $key => $value) {
	    			$fields_[$value->fields_id][$value->days_id][] = $value;
	    		}
	    	}

	    	$data['matches'] = $matches;
	    	$data['matches_per_slot'] = $matches_per_slot;
	    	$data['rounds'] = $rounds;
	    	$data['all_fields'] = $all_fields;
	    	$data['days'] = $days;
	    	$data['daysN'] = $daysN;
	    	$data['daysNa'] = $daysNa;
	    	$data['reserved_fields'] = $fields_;
	    	$data['tournaments_id'] = $id;
	    	$this->cbView('tournament_matches',$data);
	    }



	    public function getAllPossibleCouples($array){
	    	$combinations = array();
	    	foreach ($array as $x)
	    		foreach ($array as $y) {
	    			if($x != $y && !in_array($y.' '.$x,$combinations ))
	    				array_push($combinations, $x.' '.$y);
	    		}
	    		return $combinations ;
	    	}

	    	public function alternateInOffHome($array){
	    		foreach($array as $index => $item){
	    			$item_exploded = explode(' ',$item);
	    			if($index%2==0){
	    				$item_exploded = array_reverse($item_exploded);
	    				$array[$index] = implode(' ',$item_exploded);
	    			}
	    		}
	    		return $array;
	    	}
	    	public function alternateInOffHome2($array){
	    		foreach($array as $index => $item){
	    			$item_exploded = explode(' ',$item);
	    			if($index%2!=0){
	    				$item_exploded = array_reverse($item_exploded);
	    				$array[$index] = implode(' ',$item_exploded);
	    			}
	    		}
	    		return $array;
	    	}

	    	public function getNextDayFromDate($date,$day){

	    		while (date('N',strtotime($date)) != $day){
	    			$date = date('Y-m-d',strtotime($day.' +1days'));
	    		}
	    		return $date;
	    	}

	    	public function mapIntervalByDaysOfTheWeek($array){
	    		$nd= $array['begin'];
	    		while($nd<=$array['end']){
	    			$return[date('N',strtotime($nd))] = $nd;
	    			$nd = date('Y-m-d',strtotime($nd.' +1days'));
	    		}
	    		return $return;
	    	}

	    	public function checkOverlappingTimerange($b1,$e1,$b2,$e2){
	    		$b1 = strtotime($b1);
	    		$b2 = strtotime($b2);
	    		$e1 = strtotime($e1);
	    		$e2 = strtotime($e2);
	    		#echo $b1.' -> '.$e1.' vs '.$b2.' -> '.$e2.'<br>';
	    		if($e1<$b2 || $b1>$e2)
	    			return false;
	    		else
	    			return true;
	    	}
	    	public function checkOverlappingRangesAgainstInterval($ranges,$interval){
			#$this->print_rr($interval);
	    		foreach($ranges as $range){
	    			if($this->checkOverlappingTimerange($range->pause_start,$range->pause_end,$interval['begin'],$interval['end'])){
	    				return true;
	    			}
	    		}
	    	}

	    	public function AlgoritmoDiBerger($arrSquadre)
	    	{

	    		$numero_squadre = count($arrSquadre);
	    		if ($numero_squadre % 2 == 1) {
		    	    $arrSquadre[]="BYE";   // numero giocatori dispari? aggiungere un riposo (BYE)!
		    	    $numero_squadre++;
    			}
		    	$giornate = $numero_squadre - 1;
		    	/* crea gli array per le due liste in casa e fuori */
		    	for ($i = 0; $i < $numero_squadre /2; $i++)
		    	{
		    		$casa[$i] = $arrSquadre[$i];
		    		$trasferta[$i] = $arrSquadre[$numero_squadre - 1 - $i];
		    	}
		    	$return = [];
		    	for ($i = 0; $i < $giornate; $i++)
		    	{
		    		/* stampa le partite di questa giornata */
		    		#echo '<br />'.($i+1).'a Giornata<br />';
		    		/* alterna le partite in casa e fuori */
		    		if (($i % 2) == 0)
		    		{
		    			for ($j = 0; $j < $numero_squadre /2 ; $j++)
		    			{
		    				#echo ' '.$trasferta[$j].' - '.$casa[$j].'<br />';
		    				$return[$i][] = ['Home'=>$trasferta[$j],'Away'=>$casa[$j]];
		    			}
		    		}
		    		else
		    		{
		    			for ($j = 0; $j < $numero_squadre /2 ; $j++)
		    			{
		    				#echo ' '.$casa[$j].' - '.$trasferta[$j].'<br />';
		    				$return[$i][] = ['Home'=>$casa[$j],'Away'=>$trasferta[$j]];
		    			}
	    			}
				        // Ruota in gli elementi delle liste, tenendo fisso il primo elemento
				        // Salva l'elemento fisso
				    		$pivot = $casa[0];
				        /* sposta in avanti gli elementi di "trasferta" inserendo
				        all'inizio l'elemento casa[1] e salva l'elemento uscente in "riporto" */
				        array_unshift($trasferta, $casa[1]);
				        $riporto = array_pop($trasferta);
				        /* sposta a sinistra gli elementi di "casa" inserendo all'ultimo
				        posto l'elemento "riporto" */
				        array_shift($casa);
				        array_push($casa, $riporto);
				        // ripristina l'elemento fisso
				        $casa[0] = $pivot ;
				    }
				    return $return;
				}

public function roundRobin( array $teams ){

	if (count($teams)%2 != 0){
		array_push($teams,"bye");
	}
	$away = array_splice($teams,(count($teams)/2));
	$home = $teams;
	for ($i=0; $i < count($home)+count($away)-1; $i++)
	{
		for ($j=0; $j<count($home); $j++)
		{
			$round[$i][$j]["Home"]=$home[$j];
			$round[$i][$j]["Away"]=$away[$j];
		}
		if(count($home)+count($away)-1 > 2)
		{
			$s = array_splice( $home, 1, 1 );
			$slice = array_shift( $s  );
			array_unshift($away,$slice );
			array_push( $home, array_pop($away ) );
		}
	}
	return $round;
}

public function getMatchesGenerator($id) {
	$tournament = DB::table('tournaments')->where('id',$id)->first();

	    	#it's mandatory to order the pauses by theyr start day and not to set
	    	#overlapping pauses

	$pauses = DB::table('tournament_pauses')->where('tournaments_id',$id)->orderBy('pause_start','asc')->get();



	$tournament_category_costs_ = DB::table('tournament_category_costs')->where('tournaments_id',$tournament->id)->get();
	if(count($tournament_category_costs_)){
		foreach($tournament_category_costs_ as $tcc){
			$tournament_category_costs[$tcc->team_categories_id] = $tcc;
		}
	}

	$teams_ =
	DB::table('teams')
	->select('teams.*')
	->leftJoin('tournaments_teams', 'tournaments_teams.teams_id', '=', 'teams.id')
	->where('tournaments_teams.tournaments_id',$id)
	->get();

	if(count($teams_)){
		foreach($teams_ as $team){
			$teams[$team->id] = $team;
		}
	}

	$rounds =
	DB::table('tournament_rounds')
	->where('tournaments_id',$id)
	->get();

	$preferences =
	DB::table('tournaments_teams_preferences')
	->where('tournaments_id',$id)
	->get();

	if(!$preferences->isEmpty()){
		foreach($preferences as $preference){
			$pbt[$preference->teams_id][] = $preference;
		}
	}




	    	/*echo '<h1>Teams</h1>';
	    	$this->print_rr($teams);
	    	echo '<h1>Preference</h1>';
	    	$this->print_rr($preferences);
	    	echo '<h1>Gironi</h1>';
	    	$this->print_rr($rounds);*/

	    	if(count($rounds)){

	    		#clean match events
	    		DB::table('match_events')
	    		->join('matches','match_events.matches_id','=','matches.id')
	    		->join('tournament_effective_rounds','matches.tournament_effective_rounds_id','=','tournament_effective_rounds.id')
	    		->where('tournament_effective_rounds.tournaments_id',$id)
	    		->delete();
	    		#clean matches teams
	    		DB::table('matches_teams')
	    		->join('matches','matches_teams.matches_id','=','matches.id')
	    		->join('tournament_effective_rounds','matches.tournament_effective_rounds_id','=','tournament_effective_rounds.id')
	    		->where('tournament_effective_rounds.tournaments_id',$id)
	    		->delete();
	    		#clean matches
	    		DB::table('matches')
	    		->join('tournament_effective_rounds','matches.tournament_effective_rounds_id','=','tournament_effective_rounds.id')
	    		->where('tournament_effective_rounds.tournaments_id',$id)
	    		->delete();
	    		#clean rounds
	    		DB::table('tournament_effective_rounds')
	    		->where('tournament_effective_rounds.tournaments_id',$id)
	    		->delete();
	    		#clean cfos
	    		DB::table('cfos')
	    		->join('tournament_effective_rounds','cfos.tournament_effective_rounds_id','=','tournament_effective_rounds.id')
	    		->where('tournament_effective_rounds.tournaments_id',$id)
	    		->delete();


	    		$first_day = $tournament->begin_day;




	    		foreach($rounds as $rounds_index => $round){
	    			switch($round->team_categories_id){
							case 5:
								$round_compatible_teams =
								DB::table('teams')
								->select('teams.*')
								->join('tournaments_teams','teams.id','=','tournaments_teams.teams_id')
								->where('tournaments_teams.tournaments_id',$id)
								#->where('teams.team_categories_id',$round->team_categories_id)
								->where('teams.team_kinds_id',$round->team_kinds_id)
								->get();
							break;
							default:
								$round_compatible_teams =
								DB::table('teams')
								->select('teams.*')
								->join('tournaments_teams','teams.id','=','tournaments_teams.teams_id')
								->where('tournaments_teams.tournaments_id',$id)
								->where('teams.team_categories_id',$round->team_categories_id)
								->where('teams.team_kinds_id',$round->team_kinds_id)
								->get();
							break;
						}


	    		#	$this->print_rr($round_compatible_teams);

	    			#exit;

	    			$rct = [];





	    			if(count($round_compatible_teams)){
	    				foreach($round_compatible_teams as $team){
	    					$rct[] = $team->id;
	    				}
	    				$teams_per_round = round(count($rct)/$round->rounds_number);
	    				$chunks = array_filter(array_chunk($rct, $teams_per_round, true));


	    				if(count($chunks)){
	    					foreach($chunks as $chunks_index => $chunk){
	    						$last_week = [
	    							'begin'=>$first_day,
	    							'end'=> date('Y-m-d',(strtotime($first_day)+(86400*6))),
	    						];
	    						$tournament_effetive_rounds_id =
	    						DB::table('tournament_effective_rounds')
	    						->insertGetId([
	    							'tournaments_id' => $id,
	    							'team_categories_id' => $round->team_categories_id,
	    							'team_kinds_id' => $round->team_kinds_id,
	    							'round_label' => 'Girone '.($chunks_index+1)
	    						]);

	    						#associate teams to rounds

	    						foreach(array_unique($chunk) as $teams_id_){
	    							DB::table('tournament_effective_round_teams')
	    							->insert([
	    								'tournament_effective_rounds_id' => $tournament_effetive_rounds_id,
	    								'teams_id'=>$teams_id_
	    							]);
	    						}

	    						############################

	    						#GENERATE ALL POSSIBLE COUPLES
	    						$couples = $this->getAllPossibleCouples($chunk);
	    						############################

	    						#ALTERNATE MATCHES BY IN OF HOME
	    						$matches_ = $this->alternateInOffHome($couples);
	    						############################

	    						#IF THE TOURNAMENT HAS A SYMMETRICAL RETURN
	    						if($tournament->tournament_types_id	== 2){
	    							$matches_ = array_merge($matches_,$this->alternateInOffHome2($couples));
	    						}
								############################

	    						$week_matches = [];
	    						$matches_played = [];
	    						$week_teams = [];
	    						$matches_per_week = [];
	    						$turn = 1;

	    						$rounds = $this->AlgoritmoDiBerger($rct);
	    						#$this->print_rr($rounds);
	    						#exit;

	    						#$rounds = $this->roundRobin($rct);



	    						if($tournament->tournament_types_id	== 2){
	    							foreach($rounds as $round => $games){
	    								foreach($games as $game_index => $game){
	    									$rounds2[$round][$game_index]['Home'] = $game['Away'];
	    									$rounds2[$round][$game_index]['Away'] = $game['Home'];
	    								}

	    							}
	    							$rounds = array_merge($rounds,$rounds2);
	    						}



	    						foreach($rounds as $round => $games){
	    							foreach($games as $game_index => $game){
	    								$rounds_[$round][$game_index] = $game['Home'].' '.$game['Away'];
	    							}
	    						}

	    						foreach($rounds_ as $round => $games){

	    							#CHECK WEEK DOESNT INCLUDE A RESTING DAY

	    							if(count($pauses)){
	    								while($this->checkOverlappingRangesAgainstInterval($pauses,$last_week)){
	    									$last_week = [
	    										'begin' => date('Y-m-d',strtotime($last_week['begin'].' +1days')),
	    										'end' => date('Y-m-d',strtotime($last_week['end'].' +1days'))
	    									];
	    								}
	    							}

	    							#######################################

	    							$matches_per_week[] = ['matches'=>$games,'week'=>$last_week,'turn'=>$turn];
	    							$last_week = [
	    								'begin' => date('Y-m-d',strtotime($last_week['end'].' +1days')),
	    								'end' => date('Y-m-d',strtotime($last_week['end'].' +7days'))
	    							];
	    							$turn++;

	    						}


	    						/*
	    						while(count($matches_played) < count($matches_)){

	    							#CHECK WEEK DOESNT INCLUDE A RESTING DAY

	    							if(count($pauses)){
	    								while($this->checkOverlappingRangesAgainstInterval($pauses,$last_week)){
	    									$last_week = [
	    										'begin' => date('Y-m-d',strtotime($last_week['begin'].' +1days')),
	    										'end' => date('Y-m-d',strtotime($last_week['end'].' +1days'))
	    									];
	    								}
	    							}

	    							#######################################

	    							foreach($matches_ as $match){
	    								$match_teams = explode(' ',$match);
	    								if(!in_array($match,$matches_played) && !in_array($match_teams[0],$week_teams) && !in_array($match_teams[1],$week_teams)){
	    									$matches_played[] = $match;
	    									$week_matches[] = $match;
	    									$week_teams[] = $match_teams[0];
	    									$week_teams[] = $match_teams[1];
	    								}
	    							}

	    							$week_teams = [];
	    							$matches_per_week[] = ['matches'=>$week_matches,'week'=>$last_week,'turn'=>$turn];
	    							$last_week = [
	    								'begin' => date('Y-m-d',strtotime($last_week['end'].' +1days')),
	    								'end' => date('Y-m-d',strtotime($last_week['end'].' +7days'))
	    							];
	    							$week_matches = [];
	    							$turn++;
	    						}
	    						$this->print_rr($matches_per_week);*/


	    						foreach($matches_per_week as $wm){
	    							$days = $this->mapIntervalByDaysOfTheWeek($wm['week']);
		    						#$this->print_rr($days);
	    							foreach($wm['matches'] as $match){
	    								$matches_teams = explode(' ',$match);
	    								$spot_found = 0;
	    								$preference_index = 0;
	    								$team_index = 0;
	    								$count_checks = 0 ;
	    								$count_all_checks = count($pbt[$matches_teams[0]]) + count($pbt[$matches_teams[1]]);
		    							#$this->print_rr($days);
	    								while($spot_found == 0 && $count_checks<$count_all_checks){
	    									$check = DB::table('matches')
	    									->where('fields_id', $pbt[$matches_teams[$team_index]][$preference_index]->fields_id)
	    									->where('match_day',$days[$pbt[$matches_teams[$team_index]][$preference_index]->days_id])
	    									->where('time_frames_id',$pbt[$matches_teams[$team_index]][$preference_index]->time_frames_id)
	    									->get();
	    									if(count($check)){
	    										if($preference_index<(count($pbt[$matches_teams[$team_index]]))-1){
	    											$preference_index++;
	    										}else{
	    											$preference_index = 0;
	    											$team_index++;
	    										}
	    									}
	    									else{
	    										$spot_found = 1;
	    									}
	    									$count_checks++;
	    								}
	    								$insert = [
	    									'match_label' => $teams[$matches_teams[0]]->team_name.' - '.$teams[$matches_teams[1]]->team_name,
	    									'teams_id_host' => $matches_teams[0],
	    									'teams_id_guest' => $matches_teams[1],
	    									'tournament_effective_rounds_id' =>$tournament_effetive_rounds_id,
	    								];
	    								if($spot_found ==1){
	    									$match_day = $days[$pbt[$matches_teams[$team_index]][$preference_index]->days_id];
	    									$insert['fields_id'] = $pbt[$matches_teams[$team_index]][$preference_index]->fields_id;
	    									$insert['time_frames_id'] = $pbt[$matches_teams[$team_index]][$preference_index]->time_frames_id;
	    								}else{
	    									$match_day = $days[$pbt[$matches_teams[0]][0]->days_id];
	    									$insert['fields_id'] = $pbt[$matches_teams[0]][0]->fields_id;
	    									$insert['time_frames_id'] = $pbt[$matches_teams[0]][0]->time_frames_id;
	    								}
	    								$insert['match_day'] = $match_day;
		    							$insert['match_turn'] = $wm['turn'];#
		    							$matches_id = DB::table('matches')->insertGetId($insert);

		    							DB::table('matches_teams')->insert([
		    								['matches_id'=>$matches_id,'teams_id'=>$matches_teams[0]],
		    								['matches_id'=>$matches_id,'teams_id'=>$matches_teams[1]]
		    							]);

		    							DB::table('cfos')->insert([[
		    								'cfo_label' => '',
		    								'cfo_sign' => 1,
		    								'cfo_value' => $tournament_category_costs[$round->team_categories_id]->tournament_category_cost_value,
		    								'cfo_types_id' => 1, #match
		    								'cfo_date' => date('Y-m-d',strtotime($match_day.' +1days')),
		    								'cfo_confirmed' => 0,
		    								'teams_id' => $matches_teams[0],
		    								'tournament_effective_rounds_id' => $tournament_effetive_rounds_id
		    							]]);

		    							DB::table('cfos')->insert([[
		    								'cfo_label' => '',
		    								'cfo_sign' => 1,
		    								'cfo_value' => $tournament_category_costs[$round->team_categories_id]->tournament_category_cost_value,
		    								'cfo_types_id' => 1, #match
		    								'cfo_date' => date('Y-m-d',strtotime($match_day.' +1days')),
		    								'cfo_confirmed' => 0,
		    								'teams_id' => $matches_teams[1],
		    								'tournament_effective_rounds_id' => $tournament_effetive_rounds_id
		    							]]);
		    						}
		    					}
		    					#exit;


		    				}
		    			}
		    		}





		    	}
		    }
		    CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"Partite generate. Accedi al dettaglio partite.","success");
		}


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
	    	//Your code here
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */
	    public function hook_after_add($id) {
	        //Your code here
	    	DB::table('tournaments_teams')->where('tournaments_id',$id)->delete();
	    	$teams = DB::table('tournaments_teams_preferences')->where('tournaments_id',$id)->groupBy('teams_id')->get();
	    	if(count($teams)){
	    		foreach($teams as $team){
	    			DB::table('tournaments_teams')->insert([['teams_id'=>$team->teams_id,'tournaments_id'=>$id]]);
	    		}
	    	}
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_edit($id) {
	        //Your code here
	    	DB::table('tournaments_teams')->where('tournaments_id',$id)->delete();
	    	$teams = DB::table('tournaments_teams_preferences')->where('tournaments_id',$id)->groupBy('teams_id')->get();
	    	if(count($teams)){
	    		foreach($teams as $team){
	    			DB::table('tournaments_teams')->insert([['teams_id'=>$team->teams_id,'tournaments_id'=>$id]]);
	    		}
	    	}
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    public function getSetTournamentFields(){
	    	$get = $_GET;
	    	DB::table('tournaments_fields')->where('tournaments_id',$get['tournaments_id'])->delete();
	    	$fields = $get['fields'];
	    	$tournaments_id = $get['tournaments_id'];
	    	DB::table('tournaments_fields')->where('tournaments_id',$tournaments_id )->delete();
	    	if(count($fields)){
	    		foreach($fields as $field){
	    			$data = [];
	    			list($data['fields_id'],$data['days_id'],$data['time_frames_id']) = explode('_',$field);
	    			$data['tournaments_id'] = $tournaments_id;
	    			DB::table('tournaments_fields')->insert([$data]);
	    		}
	    	}
	    	CRUDBooster::redirect(CRUDBooster::mainpath(),"Campi associati al torneo con successo.","success");
	    }

	    public function getSetTournamentTeams(){
	    	$get = $_GET;

	    	$preferences = $get['preferences'];



	    	DB::table('tournaments_teams_preferences')->where('tournaments_id',$get['tournaments_id'])->delete();
	    	DB::table('tournaments_teams')->where('tournaments_id',$get['tournaments_id'])->delete();
	    	$teams = [];
	    	if(count($preferences)){
	    		foreach($preferences as $teams_id => $preference){
	    			$teams[] = $teams_id;
	    			foreach($preference as $preference_){
	    				list($pref['fields_id'],$pref['days_id'],$pref['time_frames_id']) = explode('_',$preference_);
	    				$pref['tournaments_id'] = $get['tournaments_id'];
	    				$pref['teams_id'] = $teams_id;
	    				DB::table('tournaments_teams_preferences')->insert([$pref]);
	    			}
	    		}
	    	}
	    	$teams = array_unique($teams);
	    	foreach($teams as $teams_id){
	    		DB::table('tournaments_teams')->insert([['tournaments_id'=>$get['tournaments_id'],'teams_id'=>$teams_id]]);
	    	}
	    	CRUDBooster::redirect(CRUDBooster::mainpath(),"Squadre associate al torneo con successo.","success");
	    }

	    public function getAssociateTournamentFields($id){

	    	$tournament = DB::table('tournaments')->where('id',$id)->first();
	    	$tournament_fields = DB::table('tournaments_fields')->where('tournaments_id',$id)->get();

	    	if(!$tournament_fields->isEmpty($tournament_fields)){
	    		foreach($tournament_fields as $field){
	    			$fields[$field->fields_id][$field->days_id][$field->time_frames_id] = 1;
	    		}
	    	}

	    	$this->cbView('associate_tournament_fields',[
	    		'fields'=>DB::table('fields')
	    		->select('fields.id as id','fields.field_name')
	    		->leftJoin('fields_team_types_table','fields.id','=','fields_team_types_table.fields_id')
	    		->where('fields_team_types_table.team_types_id',$tournament->team_types_id)
	    		->groupBy('fields.id')
	    		->get(),
	    		'checked_fields' => $fields,
	    		'days'=>DB::table('days')->get(),
	    		'time_frames'=> DB::table('time_frames')->get(),
	    		'tournaments_id'=>$id
	    	]);
	    }

	    public function getAssociateTournamentTeams($id){
	    	$fields_ = DB::table('tournaments_fields')
	    	->leftJoin('fields','tournaments_fields.fields_id','=','fields.id')
	    	->where('tournaments_fields.tournaments_id',$id)
	    	->get();

	    	$days_ = DB::table('days')->get();

	    	$teams_preferences_ = DB::table('tournaments_teams_preferences')->where('tournaments_id',$id)->get();
	    	if(!$teams_preferences_->isEmpty()){
	    		foreach ($teams_preferences_ as $key => $preference) {
	    			$teams_preferences[$preference->teams_id][$preference->fields_id][$preference->days_id][$preference->time_frames_id] = 1;
	    		}
	    	}else{
	    		$teams_preferences = [];
	    	}

	    	foreach($days_ as $day){
	    		$days[$day->id] = $day;
	    	}
	    	$times_frames_ = DB::table('time_frames')->get();
	    	foreach($times_frames_ as $time_frame){
	    		$time_frames[$time_frame->id] = $time_frame;
	    	}
	    	foreach($fields_ as $field){
	    		$fields[$field->id]['name'] = $field->field_name;
	    		$fields[$field->id]['days'][$field->days_id][] = $field->time_frames_id;
	    		$fields[$field->id]['count_time_frames']++;
	    	}
	    	$data = [
	    		'teams_preferences' => $teams_preferences,
	    		'teams'=>DB::table('teams')->orderBY('team_name')->get(),
	    		'fields'=>$fields,
	    		'days'=>$days,
	    		'fields_'=>$fields_,
	    		'time_frames'=>$time_frames,
	    		'tournaments_id'=>$id
	    	];
	    	$this->cbView('associate_tournament_teams',$data);

	    }



	    //By the way, you can still create your own method in here... :)


	}
