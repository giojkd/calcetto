<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;
use App\SMS;

class AdminCommunications38Controller extends \crocodicstudio\crudbooster\controllers\CBController {

	public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "communication_content";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = false;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = false;
		$this->button_export = false;
		$this->table = "communications";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"Contenuto","name"=>"communication_content"];
		
		
		
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
		$this->form[] = ['label'=>'Contenuto','name'=>'communication_content','type'=>'wysiwyg','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
		/*
		$this->form[] = ['label'=>'Torneo','name'=>'tournaments_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'tournaments,tournament_name'];
		$this->form[] = ['label'=>'Tournament Effective Rounds Id','name'=>'tournament_effective_rounds_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'tournament_effective_rounds,round_label'];
		*/
		$this->form[] = ['label'=>'Invia a tutte','name'=>'send_all','type'=>'checkbox','dataenum'=>'1|Sì'];
		$this->form[] = ['label'=>'Squadre','name'=>'teams_id','type'=>'checkbox','datatable'=>'teams,team_name'];
		#$this->form[] = ['label'=>'Squadre','name'=>'teams_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'teams,team_name'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Contenuto','name'=>'communication_content','type'=>'wysiwyg','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			//$this->form[] = ['label'=>'Torneo','name'=>'tournaments_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'tournaments,tournament_name'];
			//$this->form[] = ['label'=>'Tournament Effective Rounds Id','name'=>'tournament_effective_rounds_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'tournament_effective_rounds,round_label'];
			//$this->form[] = ['label'=>'Teams Id','name'=>'teams_id','type'=>'select2','width'=>'col-sm-10','datatable'=>'teams,team_name'];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();


	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
			*/
			
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }



	    /*
			
				Ti ricordiamo la partita di oggi 
				Audace Legnaia - Cani arrabbiati
				alle ore 21.00 
				presso Africo - campo 2. 
				Buona giornata, MSP  Calcio Firenze - https://mspcalciofirenze.it/

	    */


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here

	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */

	    public function print_rr($array){
	    	echo '<pre>';
	    	print_r($array);
	    	echo '</pre>';
	    }	    

		/**
		 * Authenticates the user given it's username and password.
		 * Returns the pair user_key, Session_key
		 */
		public function loginSMS($username, $password) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,
				'https://api.skebby.it/API/v1.0/REST/login?username=' . $username .
				'&password=' . $password);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$response = curl_exec($ch);
			$info = curl_getinfo($ch);
			curl_close($ch);

			if ($info['http_code'] != 200) {
				return null;
			}

			return explode(";", $response);
		}

		/**
		 * Sends an SMS message
		 */
		public function sendSMS($auth, $sendSMS) {

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,'https://api.skebby.it/API/v1.0/REST/sms');
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-type: application/json',
				'user_key: ' . $auth[0],
				'Session_key: ' . $auth[1]
			));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($sendSMS));
			$response = curl_exec($ch);
			$info = curl_getinfo($ch);
			curl_close($ch);

			if ($info['http_code'] != 201) {
				return null;
			}

			//echo json_decode($response);

			return json_decode($response);
		}


		public function hook_before_add(&$postdata) {        
			        //Your code here

			/*
			if($postdata['tournaments_id']>0){
				$recipients = (array)DB::table('team_contacts')->leftJoin('tournaments_teams','tournaments_teams.teams_id','=','team_contacts.teams_id')->where('tournaments_teams.tournaments_id',$postdata['tournaments_id'])->get();

			}else if($postdata['tournament_effective_rounds_id']>0){
				$recipients_host = 
				DB::table('team_contacts')
				->leftJoin('matches','matches.teams_id_host','=','team_contacts.teams_id')
				->leftJoin('tournament_effective_rounds','tournament_effective_rounds.id','=','matches.tournament_effective_rounds_id')
				->where('tournament_effective_rounds.id',$postdata['tournament_effective_rounds_id']);


				$recipients = 
				(array)DB::table('team_contacts')
				->leftJoin('matches','matches.teams_id_guest','=','team_contacts.teams_id')
				->leftJoin('tournament_effective_rounds','tournament_effective_rounds.id','=','matches.tournament_effective_rounds_id')
				->where('tournament_effective_rounds.id',$postdata['tournament_effective_rounds_id'])
				->union($recipients_host)
				->get();


			}else if($postdata['teams_id']>0){
				$recipients = 
				(array)DB::table('team_contacts')
				->where('teams_id',$postdata['teams_id'])
				->get();
			}*/

			

			if(is_array($_POST['send_all']) && in_array(1,$_POST['send_all'])){
				$recipients = (array)DB::table('team_contacts')->get();
			}else{
				$teams = $_POST['teams_id'];
				$recipients = 
				(array)DB::table('team_contacts')
				->whereIn('teams_id',$teams)
				->get();
			}

			
			$email = [];

			if(count($recipients)){
				foreach($recipients as $index => $recipient){
					foreach($recipient as $r){
						$emails[] = $r->contact_email;
						$smss[] = $r->contact_mobile_phone;
					}
				}
			}
			if(count($emails)){
			    		#communication
				foreach($emails as $email){
					CRUDBooster::sendEmail(['to'=>$email,'data'=>['communication_content'=>$postdata['communication_content']],'template'=>'communication']);
				}
			}
			if(count($smss)){

				$MESSAGE_HIGH_QUALITY = "GP";
				$MESSAGE_MEDIUM_QUALITY = "TI";
				$MESSAGE_LOW_QUALITY = "SI";

				$auth = $this->loginSMS('mgcgroup', 'Mgc2018?@@');
				$smsSent = $this->sendSMS($auth, array(
						"message" => strip_tags($postdata['communication_content']),
						"message_type" => $MESSAGE_HIGH_QUALITY,
						"returnCredits" => true,
						"recipient" => $smss,
						"sender" => 'MSP Firenze'
					));
			}


			$this->print_rr($emails);
			$postdata['communication_recipients'] = json_encode($emails);

		}

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }

	    public function sendTodayReminders(){
	    	
	    	$today = date('Y-m-d');
	    	$contacts = DB::table('team_contacts')
	    	->leftJoin('matches_teams','team_contacts.teams_id','=','matches_teams.teams_id')
	    	->leftJoin('matches','matches.id','=','matches_teams.matches_id')
	    	->leftJoin('fields','fields.id','=','matches.fields_id')
	    	->leftJoin('time_frames','time_frames.id','=','matches.time_frames_id')
	    	->select('contact_mobile_phone','match_label','field_name','time_frame_label')
	    	->where('matches.match_day',$today)
	    	->get();

	    	$sms = [];

	    	foreach($contacts as $contact){
	    		$contact->time_frame_label = substr($contact->time_frame_label, 0,5);
	    		$sms[] = [
	    			'to' => $contact->contact_mobile_phone,
	    			'message' => 'Ti ricordiamo la partita di oggi '.
	    						 $contact->match_label.
	    						 ' alle ore '.
	    						 $contact->time_frame_label.
	    						 ' presso '.
	    						 $contact->field_name.
	    						 '. Buona giornata, MSP Calcio Firenze'
	    		];
	    	}

	    	

	    	if(count($sms)>0){
	    		foreach($sms as $message){
	    			$sms_ = new Sms(['message'=>$message['message'],'recipients'=>[$message['to']]]);
	    			$sms_->send();
	    		}
	    	}
	    	
	    	

	    }



	    //By the way, you can still create your own method in here... :) 


	}