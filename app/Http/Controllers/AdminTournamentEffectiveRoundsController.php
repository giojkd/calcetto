<?php namespace App\Http\Controllers;

use Session;
use Request;
use DB;
use CRUDBooster;

class teamRoundResults{

}

class playerRoundResults{

}

class AdminTournamentEffectiveRoundsController extends \crocodicstudio\crudbooster\controllers\CBController {



	public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
		$this->title_field = "tournaments_id";
		$this->limit = "20";
		$this->orderby = "id,desc";
		$this->global_privilege = false;
		$this->button_table_action = true;
		$this->button_bulk_action = true;
		$this->button_action_style = "button_icon";
		$this->button_add = true;
		$this->button_edit = true;
		$this->button_delete = true;
		$this->button_detail = true;
		$this->button_show = true;
		$this->button_filter = true;
		$this->button_import = false;
		$this->button_export = false;
		$this->table = "tournament_effective_rounds";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
		$this->col = [];
		$this->col[] = ["label"=>"Torneo","name"=>"tournaments_id","join"=>"tournaments,tournament_name"];
		$this->col[] = ["label"=>"Categoria","name"=>"team_categories_id","join"=>"team_categories,team_category_name"];
		$this->col[] = ["label"=>"Tipo","name"=>"team_kinds_id","join"=>"team_kinds,team_kind_name"];
		$this->col[] = ["label"=>"Nome del girone","name"=>"round_label"];
		$this->col[] = ["label"=>"Fase Finale","name"=>"final_phase_round"];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
		$this->form = [];
		$this->form[] = ['label'=>'Torneo','name'=>'tournaments_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'tournaments,tournament_name'];
		$this->form[] = ['label'=>'Categoria','name'=>'team_categories_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'team_categories,team_category_name'];
		$this->form[] = ['label'=>'Tipo','name'=>'team_kinds_id','type'=>'select2','validation'=>'required','width'=>'col-sm-10','datatable'=>'team_kinds,team_kind_name'];
		$this->form[] = ['label'=>'Nome del girone','name'=>'round_label','type'=>'text','validation'=>'required','width'=>'col-sm-10'];
		$this->form[] = ['label'=>'Fase finale','name'=>'final_phase_round','type'=>'radio','width'=>'col-sm-10',"dataenum"=>"1|Attivo;0|Non attivo"];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Tournaments Id","name"=>"tournaments_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"tournaments,tournament_name"];
			//$this->form[] = ["label"=>"Team Categories Id","name"=>"team_categories_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"team_categories,team_category_name"];
			//$this->form[] = ["label"=>"Team Kinds Id","name"=>"team_kinds_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"team_kinds,team_kind_name"];
			//$this->form[] = ["label"=>"Round Label","name"=>"round_label","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
	        $this->addaction = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert        = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }

	    public function print_rr($array){
	    	echo '<pre>';
	    	print_r($array);
	    	echo '</pre>';
	    }





	    public function getDetail($id){
	    	$data = [];
	    	$round = DB::table('tournament_effective_rounds')->where('id',$id)->first();
	    	$tournament = DB::table('tournaments')->where('id',$round->tournaments_id)->first();
	    	$data['page_title'] = 'Dettaglio del girone '.$round->round_label. ' / '.$tournament->tournament_name ;


	    	$matches_ =
	    	DB::table('matches')
	    	->join('matches_teams','matches.id','=','matches_teams.matches_id')
	    	->join('teams','teams.id','=','matches_teams.teams_id')
	    	->where('matches.tournament_effective_rounds_id',$id)->get();





	    	if(count($matches_)){
	    		foreach($matches_ as $match){
	    			if(!isset($teams[$match->teams_id]))
	    				$teams[$match->teams_id] = new teamRoundResults();
	    			$teams[$match->teams_id]->team_name = $match->team_name;
	    			$teams[$match->teams_id]->team_logo = $match->team_logo;
	    			$teams[$match->teams_id]->points += $match->points;
	    			$teams[$match->teams_id]->played++;
	    			if($match->result=='win')
	    				$teams[$match->teams_id]->win ++;
	    			if($match->result=='lose')
	    				$teams[$match->teams_id]->lose ++;
	    			if($match->result=='draw')
	    				$teams[$match->teams_id]->draw ++;
	    			$teams[$match->teams_id]->scores_in += $match->scores_in;
	    			$teams[$match->teams_id]->scores_out += $match->scores_out;
	    			$teams[$match->teams_id]->scores_diff+=$match->scores_out;
	    			$teams[$match->teams_id]->scores_diff-=$match->scores_in;
	    		}
	    	}




	    	$data['teams'] = $teams;

	    	$events_ =
	    	DB::table('match_events')
	    	->leftJoin('matches','matches.id','=','match_events.matches_id')
	    	->leftJoin('players','players.id','=','match_events.players_id')
	    	->leftJoin('match_event_types','match_event_types.id','=','match_events.match_event_types_id')
	    	->where('matches.tournament_effective_rounds_id',$id)->get();



	    	$data['events'] = $events_;

	    	if(count($events_)){
	    		foreach($events_ as $event){
	    			if(!isset($players[$event->players_id]))
	    				$players[$event->players_id] = new playerRoundResults();
	    			$players[$event->players_id]->player_full_name = $event->player_full_name;
	    			if($event->match_event_types_id==1)
	    				$players[$event->players_id]->goals ++;
	    			if($event->match_event_types_id==2)
	    				$players[$event->players_id]->yellow_cards ++;
	    			if($event->match_event_types_id==3)
	    				$players[$event->players_id]->red_cards ++;
	    			if($event->match_event_types_id==4)
	    				$players[$event->players_id]->yellow_red_cards ++;
	    		}
	    	}




	    	$players = (is_array($players)) ? $players : [] ;

	    	$data['players'] = $players;

	    	$this->cbView('tournament_effective_rounds',$data);
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
	    	//Your code here
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */
	    public function hook_after_add($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_edit($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :)


	}
