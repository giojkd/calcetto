<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Le credenziali inserite non corrispondono a nessun utente.',
    'throttle' => 'Troppi tentativi di accesso falliti, riprova tra :seconds secondi.',

];
