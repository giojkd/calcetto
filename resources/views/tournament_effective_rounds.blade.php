<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class='panel panel-default'>
    <div class='panel-heading'>Classifica squadre</div>
    <div class='panel-body'>      

        <table class="table table-hover table-striped datatable">
          <thead>
            <tr>
              <th>Squadra</th>
              <th>Punti</th>
              <th>Giocate</th>
              <th>Vinte</th>
              <th>Perse</th>
              <th>Pareggiate</th>
              <th>Gol fatti</th>
              <th>Gol subiti</th>
              <th>DR</th>
            </tr>
          </thead>
          <tbody>
            @foreach($teams as $team)
            <tr>
                <td>{{$team->team_name}}</td>
                <td>{{(int)$team->points}}</td>
                <td>{{(int)$team->played}}</td>
                <td>{{(int)$team->win}}</td>
                <td>{{(int)$team->lose}}</td>
                <td>{{(int)$team->draw}}</td>
                <td>{{(int)$team->scores_in}}</td>
                <td>{{(int)$team->scores_out}}</td>
                <td>{{(int)$team->scores_diff}}</td>
            </tr>
          @endforeach

          </tbody>
          
        </table>
        
    </div>
  </div>
    <div class='panel panel-default'>
    <div class='panel-heading'>Classifica Marcatori/Cartellini</div>
    <div class='panel-body'>  
    <!--<pre> 
      <?php print_r($events)?>
      <?php print_r($players)?>
    </pre>-->
    <table class="table table-hover table-striped datatable">
          <thead>
            <tr>
              <th>Giocatore</th>
              <th>Goal</th>
              <th>Gialli</th>
              <th>Rossi</th>
              <th>Gialli/Rossi</th>
            </tr>
          </thead>
          <tbody>   
         @foreach($players as $player)
         <tr>
            <td>{{$player->player_full_name}}</td>
            <td>{{(int)$player->goals}}</td>
            <td>{{(int)$player->yellow_cards}}</td>
            <td>{{(int)$player->red_cards}}</td>
            <td>{{(int)$player->yello_red_cards}}</td>
          </tr>
         @endforeach
        
        </tbody>
      </table>
    </div>
  </div>
@endsection