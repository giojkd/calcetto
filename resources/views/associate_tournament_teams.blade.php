@extends('crudbooster::admin_template')
@section('content')
<style>
hr{
    margin:5px 0px;
} 

thead tr:first-child th { position: sticky; top: 0px; background-color: #f4f4f4}
thead tr:nth-child(2) th { position: sticky; top: 25px; background-color: #f4f4f4}
thead tr:nth-child(3) th { position: sticky; top: 50px; background-color: #f4f4f4}
</style>
<div class="panel panel-default">
    <div class="panel-heading">
        Assegna squadre al torneo
    </div>
    <form action="/admin/tournaments/set-tournament-teams">
        <input type="hidden" name="tournaments_id" value="{{$tournaments_id}}">
        <div class="container table-responsive" style="height:80vh">
        	<table class="table table-hover table-striped" id="teams-table">
                <thead>
                   <tr>
                    <th></th>
                    @foreach($fields as $fields_id => $field)
                    <th colspan="{{$field['count_time_frames']}}">
                        {{$field['name']}}
                    </th>
                    @endforeach
                </tr>
                <tr>
                    <th></th>
                    @foreach($fields as $fields_id => $field)                   
                    @foreach($field['days'] as $days_id => $time_frames_ids)
                    <th colspan="{{count($time_frames_ids)}}">
                        {{$days[$days_id]->day_name}}
                    </th>
                    @endforeach
                    @endforeach
                </tr>
                <tr>
                    <th></th>
                    @foreach($fields as $fields_id => $field)                   
                    @foreach($field['days'] as $days_id => $time_frames_ids)
                    @foreach($time_frames_ids as $time_frame_id)
                    <th>
                        {{$time_frames[$time_frame_id]->time_frame_label}}

                    </th>
                    @endforeach
                    @endforeach
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @if (count($teams_preferences) == 0)
                    <tr>
                        <td>
                            <select class="form-control team-select" required style="width:180px">
                                <option  value="">Scegli squadra...</option>
                                @foreach($teams as $team)
                                <option value="{{$team->id}}">{{$team->team_name}}</option>
                                @endforeach
                            </select>
                        </td>
                        @foreach($fields as $fields_id => $field)                   
                        @foreach($field['days'] as $days_id => $time_frames_ids)
                        @foreach($time_frames_ids as $time_frame_id)
                        <td>
                            <input name="" value="{{$fields_id}}_{{$days_id}}_{{$time_frame_id}}" type="checkbox">
                        </td>
                        @endforeach
                        @endforeach
                        @endforeach
                        <td>
                            <a onclick="$(this).closest('tr').clone().appendTo('#teams-table'); atse();" class="btn btn-success" href="javascript:"><i class="fa fa-plus"></i></a>
                            <a onclick="$(this).closest('tr').remove(); atse();" class="btn btn-warning" href="javascript:"><i class="fa fa-minus"></i></a>
                            <a onclick="$(this).closest('tr').find('input').prop('checked',true); atse();" class="btn btn-primary" href="javascript:">Tutti</a>
                            <a onclick="$(this).closest('tr').find('input').prop('checked',false); atse();" class="btn btn-primary" href="javascript:">Nessuno</a>

                        </td>
                    </tr>
                @else
                    @foreach($teams_preferences as $teams_id => $pref)
                        <tr>
                            <td>
                                <select class="form-control team-select" required style="width:180px">
                                    <option  value="">Scegli squadra...</option>
                                    @foreach($teams as $team)
                                    <option value="{{$team->id}}" @if($team->id == $teams_id) selected @endif>{{$team->team_name}}</option>
                                    @endforeach
                                </select>
                            </td>
                            @foreach($fields as $fields_id => $field)                   
                            @foreach($field['days'] as $days_id => $time_frames_ids)
                            @foreach($time_frames_ids as $time_frame_id)
                            <td>
                                <input @if($pref[$fields_id][$days_id][$time_frame_id] == 1) checked @endif name="" value="{{$fields_id}}_{{$days_id}}_{{$time_frame_id}}" type="checkbox">
                            </td>
                            @endforeach
                            @endforeach
                            @endforeach
                            <td>
                                <a onclick="$(this).closest('tr').clone().appendTo('#teams-table'); atse();" class="btn btn-success" href="javascript:"><i class="fa fa-plus"></i></a>
                                <a onclick="$(this).closest('tr').remove(); atse();" class="btn btn-warning" href="javascript:"><i class="fa fa-minus"></i></a>
                                <a onclick="$(this).closest('tr').find('input').prop('checked',true); atse();" class="btn btn-primary" href="javascript:">Tutti</a>
                                <a onclick="$(this).closest('tr').find('input').prop('checked',false); atse();" class="btn btn-primary" href="javascript:">Nessuno</a>

                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        <button class="btn  btn-success">Salva</button>
    </div>

</form>
</div>
@push('bottom')
<script type="text/javascript">
    function atse(){
        $('.team-select').change(function(){
            var tr = $(this).closest('tr');
            var name = "preferences["+$(this).val()+"][]";
            tr.find('input[type="checkbox"]').attr('name',name);
        })
    }
    $('document').ready(function(){
        atse();
        $('.team-select').trigger('change');
    })
    
    
</script>
<style>
    #teams-table tbody tr td,#teams-table thead tr th{
        border-right: 1px solid;
    }
    #teams-table tbody tr td,#teams-table thead tr th{
        border-color:#999;
    }
</style>
@endpush
@endsection