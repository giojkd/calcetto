<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
<!-- Your html goes here -->
<form method='post' action='{{CRUDBooster::mainpath('add-save')}}'>
  @csrf
  <input type="hidden" name="fields_id" value="">
  <input type="hidden" name="time_frames_id" value="">
  <div class='panel panel-default'>
    <div class='panel-heading'>Add Form</div>
    <div class='panel-body'>

      <div class='form-group'>
        <label>Tipo</label>
        <select class="form-control select2" id="teamTypeId" name="team_types_id" onchange="loadFieldOptions()">
          @foreach($teamTypes as $type)
          <option value="{{$type->id}}">{{$type->team_type_name}}</option>
          @endforeach
        </select>
      </div>
      <div class='form-group'>
        <label>Day</label>
        <input type="text" class="form-control datepicker_custom datepicker_custom_first" name="match_day" value="{{date('Y-m-d')}}" id="matchDay">
      </div>
      <div class="form-group">
        <label for="">Campo</label>
        <div class="" style="max-height: 320px; overflow-y: scroll">
          <table id="fieldsOptionsTable" class="table table-hover table-striped">

          </table>
        </div>
      </div>
      <div class="form-group">
        <label for="">Nome</label>
        <input type="text" name="name" class="form-control" value="">
      </div>
      <div class="form-group">
        <label for="">Cognome</label>
        <input type="text" name="surname" class="form-control" value="">
      </div>
      <div class="form-group">
        <label for="">Telefono</label>
        <input type="text" name="telephone" class="form-control" value="">
      </div>
      <div class="form-group">
        <label for="">Email</label>
        <input type="text" name="email" class="form-control" value="">
      </div>
      <div class="form-group">
        <label for="">Tipo di prenotazione</label>
        <select class="form-control" name="reservation_type" onchange="if($(this).val() == 'recursive'){$('#recursiveReservationUntilDate').removeClass('hidden')} else {$('#recursiveReservationUntilDate').addClass('hidden')}">
          <option value="single">Singola</option>
          <option value="recursive">Tutte le settimane fino a...</option>
        </select>
      </div>
      <div class="form-group hidden"  id="recursiveReservationUntilDate">
        <label for="">Prenota tutte le settimane fino a</label>
        <input type="text" name="reservation_until" class="form-control datepicker_custom" onchange="checkAvailability()">
      </div>

      <div class="alert alert-warning hidden" id="errorsPanel">
        <h3>Attenzione! Alcune delle date sono già occupate</h3>
        <ul class="list-unstyled">


        </ul>
      </div>
    </div>
    <div class='panel-footer'>
      <input type='submit' class='btn btn-primary' value='Save'/>
    </div>
  </div>
</form>
@push('bottom')
<script type="text/javascript">

  function checkAvailability(){
      $.post('{{CRUDBooster::mainpath('check-availability')}}',
      {
        from:$('input[name="match_day"]').val(),
        until:$('input[name="reservation_until"]').val(),
        time_frames_id:$('input[name="time_frames_id"]').val(),
        fields_id:$('input[name="fields_id"]').val(),
      },
    function(r){
      $('#errorsPanel .list-unstyled').html('')
      if(r.busyDays.length > 0){
        $('#errorsPanel').removeClass('hidden');
        console.log(r.busyDays);
        for(var i in r.busyDays){
          $('#errorsPanel .list-unstyled').append('<li>'+r.busyDays[i]+'</li>');
        }
      }else{
        $('#errorsPanel').addClass('hidden');
      }
    },'json')
  }

function setFieldAndTimeOption(val){

  var valArray = val.val().split('-');
  $('input[name="fields_id"]').val(valArray[0]);
  $('input[name="time_frames_id"]').val(valArray[1]);
  console.log(valArray);
}
function loadFieldOptions(){
  var teamTypeId = $('#teamTypeId').val();
  var matchDay = $('#matchDay').val();
  $.get('/api/get-fields',{team_types_id:teamTypeId,match_day:matchDay},function(r){
    $('#fieldsOptionsTable').html('');
    if(r.length > 0){
      for(var i in r){
        //console.log(r[i]);
        var fieldRow = new $('<tr><td><label><input onchange="setFieldAndTimeOption($(this))" required type="radio" value="'+r[i].fields_id+'-'+r[i].time_frames_id+'" name="field_option"> '+r[i].field_name+' -> '+r[i].time_frame_label+'</label></td></tr>');
        $('#fieldsOptionsTable').append(fieldRow);
      }
    }else{
      $('#fieldsOptionsTable').html('<div class="alert alert-info">Nessun campo disponibile</div>');
    }
  },'json')

}
$(function(){
  loadFieldOptions();
  $('.datepicker_custom').datepicker({
    format:'yyyy-mm-dd',

  });
  $('.datepicker_custom_first').datepicker()
  .on('changeDate', function(e) {
    loadFieldOptions();
  });
})
</script>
@endpush
@endsection
