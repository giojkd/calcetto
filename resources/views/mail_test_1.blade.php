<html>
<head>
	<style type="text/css">
		table.main-table{
			font-size: 13px;
		}
		.legend{
			font-size: 13px;
		}
	</style>
</head>
<body style="padding:0px; margin:0px;">
	<table style="width:100%">
		<tr>
			<td style="text-align:center; width:25%"><img style="height:65px" src="<?php echo $_SERVER["DOCUMENT_ROOT"].'/img/logo_msp.png';?>"></td>
			<td style="text-align:center; width:50%">
				<h4 style="margin:0px 0px 5px 0px;">MSP CALCIO FIRENZE</h4>
				<h5 style="margin:0px 0px 5px 0px;">ORGANIZZAZIONE TORNEI E CAMPIONATI C5/C7/C11 | www.mspcalciofirenze.it</h5>
			</td>
			<td style="text-align:center; width:25%"><img style="height:65px" src="<?php echo $_SERVER["DOCUMENT_ROOT"].'/img/logo_msp.png';?>"></td>
				</tr>
			</table>
			<table style="width:100%">
				<tr>
					<td>
						<h1 style="text-align: center; margin:0px 0px 5px 0px;">{{$team_name}}</h1>
					</td>
				</tr>
			</table>
			<table style="width:100%">
				<tr>
					<td style="width:25%">Distinta dei giocatori:</td>
					<td style="width:75%; text-align: center; border:1px solid black;"><b>{{$match_label}}</b></td>
				</tr>
			</table>
			<table style="width:100%">
				<tr>
					<td style="width:25%">Manifestazione</td>
					<td style="width:40%; text-align: center; border:1px solid black;">
						<h5 style=" margin:5px 0px">{{$tournament_name}}</h5>
					</td>
					<td style="width:35%; text-align: center; border:1px solid black;">{{$round_label}}</td>
				</tr>
			</table>
			<table style="width:100%">
				<tr>
					<td style="width:25%">Da disputare il</td>
					<td style="width:20%; text-align: center; border:1px solid black;">{{$match_day}}</td>
					<td style="width:10%; text-align: center;">Ore</td>
					<td style="width:20%; text-align: center; border:1px solid black;">{{$time_frame_start}}</td>
					<td style="width:25%; text-align: right;"><h4 style=" margin:5px 0px">{{$field_name}}</h4></td>
				</tr>
			</table>
			<table style="width:100%" class="main-table">
				<tr>
					<th style="text-align: center; width: 10%">RETI</th>
					<th style="text-align: center; width: 4%">A</th>
					<th style="text-align: center; width: 4%">E</th>
					<th style="text-align: center; width: 5%">N</th>
					<th style="text-align: center; padding:0px; width: 54%">Giocatori/Dirigenti</th>
					<th style="text-align: center; padding:0px; width: 5%">Giorno</th>
					<th style="text-align: center; padding:0px; width: 5%">Mese</th>
					<th style="text-align: center; padding:0px; width: 5%">Anno</th>
					<th style="text-align: center; padding:0px; width: 10%">Tessera</th>
					<th style="text-align: center; padding:0px; width: 4%">Cap</th>
					<th style="text-align: center; padding:0px; width: 4%">V Cap</th>
				</tr>

				@foreach ($players as $player)

				<tr>
					<td style="height:20px;  text-align: center; border:1px solid black; "></td>
					<td style="text-align: center; border:1px solid black; "></td>
					<td style="text-align: center; border:1px solid black; "></td>
					<td style="text-align: center; border:1px solid black; "></td>
					<td style="text-align: center; border:1px solid black; white-space: nowrap; ">{{$player->player_last_name}} {{$player->player_first_name}}</td>
					<?php $player->player_date_of_birth = date('d-m-Y',strtotime($player->player_date_of_birth))?>
					@foreach(explode('-', $player->player_date_of_birth) as $info) 
					<td style="text-align: center; border:1px solid black;">{{$info}}</td>
					@endforeach
					<td style="text-align: center; border:1px solid black; ">{{$player->	player_licensee_number}}</td>
					<td style="text-align: center; border:1px solid black; "></td>
					<td style="text-align: center; border:1px solid black; "></td>
				</tr>
				@endforeach

				
				@for ($i = count($players); $i < 22; $i++)
				<tr>
					<td style="height:20px; text-align: center; border:1px solid black;"></td>
					<td style="text-align: center; border:1px solid black;"></td>
					<td style="text-align: center; border:1px solid black;"></td>
					<td style="text-align: center; border:1px solid black;"></td>
					<td style="text-align: center; border:1px solid black;"></td>
					<td style="text-align: center; border:1px solid black;"></td>
					<td style="text-align: center; border:1px solid black;"></td>
					<td style="text-align: center; border:1px solid black;"></td>
					<td style="text-align: center; border:1px solid black;"></td>
					<td style="text-align: center; border:1px solid black;"></td>
					<td style="text-align: center; border:1px solid black;"></td>
				</tr>
				@endfor
				
			</table>
			<table style="width:100%">
				<tr>
					<td style="width:35%; ">
						<h5 style="text-align: center; text-decoration: underline; margin:0px 0px 5px 0px;">IL DIRIGENTE RESPONSABILE</h5>
						<div style="border-bottom: 1px solid black; width: 100%; margin:15px 0px">Firma</div>
					</td>
					<td style="width:35%; text-align: center;">
						<h5 style="text-align: center; text-decoration: underline; margin:0px 0px 5px 0px;">Cognome e nome</h5>
						<div style="border-bottom: 1px solid black; width: 100%; color: white; margin:15px 0px">.</div>
					</td>
					<td style="width:15%; text-align: center;">
						<h5 style="text-align: center; text-decoration: underline; margin:0px 0px 5px 0px;">Data di nascita</h5>
						<div style="border-bottom: 1px solid black; width: 100%; color: white; margin:15px 0px">.</div>
					</td>
					<td style="width:15%; text-align: center;">
						<h5 style="text-align: center; text-decoration: underline; margin:0px 0px 5px 0px;">Numero tessera</h5>
						<div style="border-bottom: 1px solid black; width: 100%; color: white; margin:15px 0px">.</div>
					</td>
				</tr>
			</table>
			<table style="width:100%">
				<tr>
					<td style="width:100%" class="legend">
						Legenda : <strong>( RETI )</strong> Numero reti segnate <strong>( A )</strong> Ammonito <strong>( E )</strong> Espulso <strong>( N )</strong> Numero Maglia
					</td>
				</tr>
			</table>
			<table style="width: 100%">
				<tr>
					<td style="text-align: center">
						<h5 style="text-decoration: underline; font-style: italic; margin:0px 0px 5px 0px;">
							Spazio riservato al D.G.
						</h5>
					</td>
				</tr>
			</table>
			<table style="width: 100%">
				<tr>
					<td style="width:16.6%"><h6 style="text-decoration: underline; font-style: italic; margin:0px 0px 5px 0px;">Reti segnate</h6></td>
					<td style="width:16.6%;height:10px; border:1px solid black;">Cifre</td>
					<td style="width:16.6%"><h6 style="text-decoration: underline; font-style: italic; margin:0px 0px 5px 0px;">Nei supplementari</h6></td>
					<td style="width:16.6%;height:10px; border:1px solid black;">Cifre</td>
					<td style="width:16.6%"><h6 style="text-decoration: underline; font-style: italic; margin:0px 0px 5px 0px;">Ai calci di rigore</h6></td>
					<td style="width:16.6%;height:10px; border:1px solid black;">Cifre</td>
				</tr>
			</table>
			<table style="width: 100%">
				<tr>
					<td>
						<h5 style="margin:0px 0px 5px 0px;">
							Motivazione delle espulsioni
						</h5>
					</td>
				</tr>
			</table>
			<table style="width:100%">
				<tr>
					<td style="width:100%; height:15px; border:1px solid black;color:white;"></td>
				</tr>	
				<tr>
					<td style="width:100%; height:15px; border:1px solid black;color:white;"></td>
				</tr>
			</table>
			<table style="width:100%">
				<tr>
					<td style="width: 70%"></td>
					<td style="width:30%; text-align: right">
						L'arbitro
						<div style="border-bottom: 1px solid black; height:25px"></div>
					</td>
				</tr>	
			</table>
	</body>
</html>



