@extends('crudbooster::admin_template')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Calendario delle partite
        </div>
        <form method="POST" action="{{CRUDBooster::mainpath('add-save')}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="panel-body">
	            @foreach($rounds as $round)
	            <h2>{{$round->round_label}}</h2>
	            <div class="table-responsive">
	                <table class="table table-striped table-hover">
	                    <thead>
	                        <tr>
	                            <th>
	                            	Campo
	                            </th>

	                            @foreach ($daysN as $index => $day)
	                           
	                                <th class="text-center @if ($daysNa[$index] =='Dom') bl1b @endif">
	                                	
	                                			
	                                	
	                                    {{$day}}
	                                	
	                                </th>
	                                 @if($daysNa[$index] =='Dom')
									 		<th>
												Campo
											</th>
									 	@endif
	                            @endforeach
	                        </tr>
	                    </thead>
	                    
	                    <tbody>
	                       <?php foreach($all_fields as $field){?>
								<tr>
									<td>
										<?php echo $field->field_name?>
									</td>
									 <?php foreach($days as $index => $day){?>
									 	
										<td class="text-center @if ($daysNa[$index] =='Dom') bl1b @endif">
											<?php 

											$day_of_the_week = date('N',strtotime($day));
											if(is_array($reserved_fields[$field->fields_id][$day_of_the_week])){
												$rsfs = $reserved_fields[$field->fields_id][$day_of_the_week];
												foreach($rsfs as $rsf){
													?>
													<?php echo $rsf->time_frame_label?>
													<div 
													class="matches-holder slot__<?php echo $field->fields_id?>__<?php echo $day;?>__<?php echo $rsf->time_frames_id;?> <?php if($matches_per_slot[$field->fields_id][$day][$rsf->time_frames_id]>1) echo 'warning'?>" 
													data-fields_id="<?php echo $field->fields_id?>"
													data-day="<?php echo $day;?>"
													data-time_frames_id="<?php echo $rsf->time_frames_id;?>"
													data-tournaments_id="<?php echo $tournaments_id;?>"
													>
														<?php 
														if(is_array($matches[$round->id][$field->fields_id][$day][$rsf->time_frames_id])){
															foreach($matches[$round->id][$field->fields_id][$day][$rsf->time_frames_id] as $match){
																?>
																<div data-matches_id = "<?php echo $match->id?>"  class="match">
																	<?php echo $match->match_label?>
																</div>
																<?php
															}
														}
													?>
													</div>
													<?php
												}
											}
											?>
										</td>
										@if($daysNa[$index] =='Dom')
									 		<td>
												<?php echo $field->field_name?>
											</td>
									 	@endif
									 <?php
										}
									 ?>
								</tr>	
	                       <?php
	                   		}
	                       ?>
	                    </tbody>	                    
	                </table>
	            </div>
	                <hr>
	            @endforeach
            </div>
            <div class="panel-footer">
                
            </div>
        </form>
    </div>
    @push('bottom')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>

		function drop_match($match,droppable){
			$match
			.css({left:'0px',top:'0px'})
			.appendTo(droppable);
			var data = droppable.data();

			$.get('/admin/tournaments/set-match-slot/'+$match.data().matches_id+'/'+data.fields_id+'/'+data.day+'/'+data.time_frames_id+'/'+data.tournaments_id,{},function(slots){
				$('.matches-holder').removeClass('warning');
				if(slots.length>0){
					for(var i in slots){
						$('.slot__'+slots[i].fields_id+'__'+slots[i].match_day+'__'+slots[i].time_frames_id).addClass('warning');
					}
				}
			},'json');
			
			
		}
		$(function(){
			
			var $holders = $( ".matches-holder" );
		 
		    // Let the gallery items be draggable
		    $( ".match", $holders ).draggable({
		      containment: "document",
		      cursor: "move",
		      revert: "invalid",
		    });

		    $holders.droppable({
		      accept: ".match",
		      classes: {
		        
		      },
		      drop: function( event, ui ) {
		        drop_match(ui.draggable,$(this));
		      }
    		});	
		})
    </script>
    @endpush
    <style>

    	.match.ui-draggable{
    		min-width: 150px;
    	}

    	.matches-holder{
    		border:1px solid green;
    		border-radius: 4px;
    		padding:5px;
    		text-align: center;
    		margin-bottom: 2px;
    		min-height: 15px;
    		min-width: 50px;
    	}

    	.matches-holder.warning{
    		border-color:red;
    	}

    	.table-striped>tbody>tr:nth-of-type(odd){
    		background-color:#eee!important;
    	}

    	.matches-holder .match{
			padding:2px;
			border-radius: 2px;
			border: 1px solid #ccc;
			margin-bottom: 2px;
			cursor: move;
    	}
    	.matches-holder .match:hover{
    		background-color: #ccc;
    	}
    	.bl1b{
		border-right:1px solid black;
	}
    </style>
@endsection
