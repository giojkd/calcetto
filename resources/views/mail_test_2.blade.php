<h2 style="text-align:center; width:100%; border:1px solid black; margin:5px 0px 5px 0px">Gare da {{$matches_from}} a {{$matches_to}}</h2>		
@foreach ($match_groups as $day => $match_group)
	<table style="width:100%">
				<tr>
					<td style="width:50%">
						<h2 style="border:1px solid black; display: inline-block; margin:5px 0px 5px 0px">{{$day}}</h2>
					</td>
				</tr>
			</table>
	@foreach($match_group as $field_name => $matches)
			
			<table style="width:100%">
				<tr>
					<td style="width:50%" colspan="3">
						<h3 style="border:1px solid black; color:#fff; background-color:#999; margin:5px 0px 5px 0px">{{$field_name}} | {{$matches[0]->referee_full_name}} | {{$matches[0]->delegate_full_name}}</h3>
					</td>
				</tr> 
				@foreach($matches as $index => $match)
					<tr>
						<td style="width:15%;border:1px solid black; font-size: 13px;">{{$match->time_frame_start}}</td>
						<td style="width:35%;border:1px solid black; font-size: 13px; white-space: nowrap;">{{$match->match_label}}</td>
						<td style="width:35%;border:1px solid black; font-size: 13px;">{{$match->tournament_name}}</td>
					</tr>
				@endforeach
			</table>
	@endforeach
@endforeach