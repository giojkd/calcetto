<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
	<title>Risultati</title>
	<style type="text/css">
		.player{
			border: 1px solid #ececec;
			margin:15px 0px;
			border-radius: 4px;
			padding: 10px;
		}
		body {
		font-size: 13px !important;
		font-family: 'Roboto Condensed', sans-serif;
	}


	a {
		color: #054c89 !important;
	}

	h4{
		font-size: 29px;
		font-weight: 600;
		color: #054c89 !important;
	}
	</style>
</head>
<body class="pt-3">

	<div id="app">
		<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<a href="http://mspcalciofirenze.it"><img class="img-fluid mb-2" src="http://mspcalciofirenze.it/wp-content/uploads/2018/08/logo_msp-1.png"></a>
						
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<a class="btn btn-info mb-2" href="http://mspcalciofirenze.it"><i class="fa fa-chevron-circle-left"></i> Torna indietro</a>
					</div>
				</div>
			</div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h4>Squadre</h4>
				</div>
			</div>
			<input class="form-group" v-model="search1" placeholder="Ricerca squadra"></input>
			<div class="row" v-if="search1 != ''">
				<div v-for="team in teamsList" class="col-md-3 col-xs-6">
					<a v-bind:href="'/front#/teams/'+team.id">{{team.team_name}}</a>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-12">
					<h4>Giocatori</h4>
				</div>
			</div>
			<input class="form-group" v-model="search" placeholder="Ricerca giocatore"></input>
			<div class="row">
				<div v-for="player in playersList" class="col-md-3 col-xs-6" v-if="search != ''">
					<div class="player">
						<b>{{player.player_full_name}}</b>
						<p class="font-weight-light">
							{{player.player_licensee_number}}<br/>
							{{player.teams}}
						</p>
						 
					</div>
				</div>
			</div>
		</div>



	</div>

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://unpkg.com/vue-router@2.0.0/dist/vue-router.js"></script>
	<script type="text/javascript">
		

		var app = new Vue({
			

			el: '#app',
			data: {
				teams:[],
				players:[],
				search:'',
				search1:'',
			},
			created(){
				fetch('/front/get-teams')
				.then(response=>response.json())
				.then(json=>{
					this.teams = json.teams;
					this.players = json.players;
				})
				
			},
			watch:{

			},
			computed: {
				teamsList(){
					return this.teams.filter(team => {
			        return team.team_name.toLowerCase().includes(this.search1.toLowerCase())
			      })
				},
			    playersList() {
			      return this.players.filter(player => {
			        return player.player_full_name.toLowerCase().includes(this.search.toLowerCase())
			      })
			    }
			  },
			methods:{

			}
		})

	</script>
</body>