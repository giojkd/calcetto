@extends('crudbooster::admin_template')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            Designa arbitri
        </div>
      
    
            <div class="panel-body table-responsive">
                  <form class="form form-inline" action="/admin/match_referee/add">
                        <div class="form-group">
                            <input name="firstDate" class="form-control datepicker" value="{{$firstDate}}"></input>
                        </div>
                        <button class="btn btn-success" type="submit">Cerca</button>
                    </form>
                    <form method="POST" action="{{CRUDBooster::mainpath('add-save')}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th></th>
                            @foreach ($days as $day)
                                <th>
                                    {{$day}}
                                </th>
                            @endforeach
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($fields as $field)
                            <tr>
                                <td>
                                    {{$field->field_name}}
                                </td>
                                @foreach ($days as $day)
                                <td>
                                    @foreach ($matches as $match)
                                        @if($match->fields_id == $field->id && $match->match_day == $day)
                                                {{$match->match_label}} ({{$match->time_frame_label}})
                                                <form method="POST" action="{{CRUDBooster::mainpath('add-save')}}">
                                                    <input type="hidden" name="matches_id" value="{{$match->matches_id}}">
                                                    <input type="hidden" name="uncazzo" value="si">
                                                    <div class="form-group">
                                                        <select name="referees_id" id="" class="form-control onchange-submit">
                                                            <option value="">Scegli arbitro...</option>
                                                            @foreach($referees as $referee)
                                                                <option @if($match->referees_id == $referee->id) selected @endif value="{{$referee->id}}">{{$referee->referee_full_name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <select name="delegates_id" id="" class="form-control onchange-submit">
                                                            <option value="">Scegli delegato...</option>
                                                            @foreach($delegates as $delegate)
                                                                <option @if($match->delegates_id == $delegate->id) selected @endif value="{{$delegate->id}}">{{$delegate->delegate_full_name}} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </form>
                                        @endif
                                    @endforeach
                                </td>
                                @endforeach
                            <tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
                </form>
            </div>
            <div class="panel-footer">
               <!-- <button class="btn-primary btn" type="submit">Salva</button> -->
            </div>
        
    </div>
@endsection