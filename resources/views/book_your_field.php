<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/it.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
	<link rel='stylesheet' href='https://unpkg.com/v-calendar/lib/v-calendar.min.css'>
	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
	<title>Prenota il tuo campo</title>
	<style type="text/css">



		.player{
			border: 1px solid #ececec;
			margin:15px 0px;
			border-radius: 4px;
			padding: 10px;
		}
		body {
		font-size: 13px !important;
		font-family: 'Roboto Condensed', sans-serif;
	}


	a {
		color: #054c89 !important;
	}

	h4{
		font-size: 29px;
		font-weight: 600;
		color: #054c89 !important;
	}


	body {
		font-size: 13px !important;
		font-family: 'Roboto Condensed', sans-serif !important;
	}


	a {
		color: #054c89 !important;
	}

	h4{
		font-weight: 600;
		font-size: 21px;
	    font-weight: 600;
	    color: #fff !important;
	    background-color: #054c89 !important;
	    padding: 8px;
	}

	h3{
		color:#054c89 !important;
	}


	.btn-msp{
		background: #054c89;
    color: white!important;
    border-radius: 0px;
	}


</style>
</head>
<body class="pt-3">
	<div id="app">
		<div class="container">
			<div class="col-12">
				<h3>Prenota il tuo Campo</h3>
				<form @submit="newReservation" v-if="fields_reservations_id == ''">
					<div class="row">
						<div class="col-md-4 col-xs-12">
							<div class="form-group">
								<select v-model="formData.team_types_id" class="form-control" v-on:change="loadFields">
									<option value="">Tipo di campo</option>
									<option v-bind:value="game.id" v-for="game in games">
										{{game.team_type_name}}
									</option>
								</select>
							</div>

						</div>
						<div class="col-md-4 col-xs-12">
							<div class="form-group">
								<v-date-picker   :input-props='{ class: "form-control", placeholder: "Scegli il giorno", readonly: true }'  mode='single' v-model='selectedDate'></v-date-picker>
							</div>
						</div>
						</div>
						<div v-if="formData.team_types_id!=''" class="row">
							<div
								v-if="fields.length > 0"
								class="field-wrapper card mb-2 col-md-6 col-xs-12"
								v-for="field in fields"
								v-bind:id="'field_'+field.fields_id+'_'+field.time_frames_id"
							>

								<div class="card-body">
									<h3 class="card-title">{{field.field_name}} </h3>

									<p>{{field.field_address}} | Costo: €{{(field.field_cost!=null) ? field.field_cost.toFixed(2) : ''}}</p>
									<a
									href="javascript:"
									v-if="fields_id==''"
									v-on:click="selectField(field.fields_id,field.time_frames_id)"
									class="btn btn-msp">Scegli {{field.time_frame_label}}</a>
									<a href="javascript:" v-if="fields_id!=''" v-on:click="selectField('','')" class="btn btn-msp">Cancella</a>
								</div>
							</div>
						</div>
					</div>
					<div v-if="fields_id!=''">
						<h4>Completa il form per prenotare</h4>
						<div class="row">
							<div class="form-group col">
								<label>Nome</label>
								<input v-model="formData.name" class="form-control" required placeholder="Obbligatorio"></input>
							</div>
							<div class="form-group col">
								<label>Cognome</label>
								<input v-model="formData.surname" class="form-control" required placeholder="Obbligatorio"></input>
							</div>
						</div>
						<div class="row">
							<div class="form-group col">
								<label>Telefono</label>
								<input v-model="formData.telephone" class="form-control" required placeholder="Obbligatorio"></input>
							</div>
							<div class="form-group col">
								<label>Email</label>
								<input v-model="formData.email" class="form-control" type="email" required placeholder="Obbligatorio"></input>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<button @click="newReservation" class="btn btn-msp">Conferma</button>
							</div>
						</div>
					</div>
				</form>
				<div class="alert alert-info" v-if="fields_reservations_id!=''">
					Grazie, per questo è il codice della tua prenotazione {{fields_reservations_id}}
				</div>
			</div>
		</div>

	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://unpkg.com/vue-router@2.0.0/dist/vue-router.js"></script>
	 <script src='https://unpkg.com/vue/dist/vue.js'></script>
    <!--3. Link VCalendar Javascript (Plugin automatically installed)-->
    <script src='https://unpkg.com/v-calendar'></script>
	<script type="text/javascript">
		var app = new Vue({

					el: '#app',
					data: {
						games:[],
						fields:[],
						team_types_id:'',
						fields_id:'',

          				selectedDate: null,
          				formData:{
          					team_types_id:''
          				},
          				fields_reservations_id:''
					},
					created(){
						fetch('/api/get-games')
						.then(response=>response.json())
						.then(json=>{
							this.games = json;
							console.log(json);

						})
					},
					mounted(){

					},
					watch:{
						fields_id:function(){

						},
						selectedDate:function(){
							this.loadFields();
						}
					},
					computed:{
						match_day:function(){
							var md = moment(this.selectedDate).format('YYYY-MM-DD');
							this.formData.match_day = md;
							return md;
						}
					},
					methods:{

						newReservation(e){
							e.preventDefault();
							axios.post('/api/new-field-reservation', this.formData)
							  .then(json => {
							    this.fields_reservations_id = json.data.fields_reservations_id;
							  })
						},

						selectField(fields_id,time_frames_id){
							this.formData.fields_id = fields_id;
							this.formData.time_frames_id = time_frames_id;
							this.fields_id = fields_id;
							this.time_frames_id = time_frames_id;
							if(fields_id!=''){
								$('.field-wrapper').hide();
								$('#field_'+fields_id+'_'+time_frames_id).show();
							}else{
								$('.field-wrapper').show();
							}
						},
						loadFields(){
							fetch('/api/get-fields?team_types_id='+this.formData.team_types_id+'&match_day='+this.match_day)
							.then(response=>response.json())
							.then(json=>{
								this.fields = json;
								console.log(json);

							})
						},
						openCalendar(){

						}
					}
				})
					</script>

</body>
</html>
