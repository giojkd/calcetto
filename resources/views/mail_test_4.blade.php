<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/it.js"></script>
	<title>Risultati</title>
	<style type="text/css">
		
	body {
		font-size: 13px !important;
		font-family: 'Roboto Condensed', sans-serif !important;
	}
	</style>
</head>
<body class="pt-3">
	<div class="container">
				<div class="row">

					<div class="col-12 text-center">
						<a href="http://mspcalciofirenze.it"><img class="img-fluid mb-2" src="http://mspcalciofirenze.it/wp-content/uploads/2018/08/logo_msp-1.png"></a>
						
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<a class="btn btn-info mb-2" href="http://mspcalciofirenze.it"><i class="fa fa-chevron-circle-left"></i> Torna indietro</a>
					</div>
				</div>
				
			</div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						
					
@foreach ($match_groups as $day => $match_group)
	<table class="table table-hover table-striped" style="width:100%">
				<tr>
					<th colspan="2" class="text-center">
						 <h4>{{$day}}</h4>
					</th>
				</tr>
			
	@foreach($match_group as $field_name => $matches)
			
			
				<tr>
					<th colspan="2">
						{{$field_name}} | {{$matches[0]->referee_full_name}} | {{$matches[0]->delegate_full_name}}
					</th>
				</tr> 
				@foreach($matches as $index => $match)
					<tr>
						<td >{{$match->time_frame_start}}</td>
						<td>{{$match->match_label}}</td>
					</tr>
				@endforeach
			
	@endforeach
	</table>
@endforeach
</div>
				</div>

			</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
	<script src="https://unpkg.com/vue-router@2.0.0/dist/vue-router.js"></script>
</body>
</html>