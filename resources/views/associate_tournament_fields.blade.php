@extends('crudbooster::admin_template')
@section('content')
<?php
    
?>
<style>
hr{
    margin:5px 0px;
}
</style>
<div class="panel panel-default">
    <div class="panel-heading">
        Assegna campi al torneo
    </div>
    
    <form action="/admin/tournaments/set-tournament-fields">
        <input type="hidden" name="tournaments_id" value="{{$tournaments_id}}">
        <div class="container table-responsive">
        	<table class="table table-hover table-striped" id="teams-table">
                <thead>
                   
            </thead>
            <tbody>
                <tr>
                    @foreach($days as $day)
                        <tr>
                    <th></th>
                    @foreach($fields as $field)
                    <th>
                        {{$field->field_name}}
                    </th>
                    @endforeach
                </tr>
                    	<tr>
                    		<td>
                    			{{$day->day_name}}
                    		</td>
                    		@foreach($fields as $field)
			                    <th>
			                    	<ul class="list-unstyled list">
			                        @foreach($time_frames as $time_frame)
			                        	<li>
			                        		<label><input <?php echo($checked_fields[$field->id][$day->id][$time_frame->id]==1) ? 'checked' : '' ?> name="fields[]" value="{{$field->id}}_{{$day->id}}_{{$time_frame->id}}" type="checkbox"> {{$time_frame->time_frame_label}}</label>
			                        	</li>
			                        @endforeach
			                        </ul>
			                    </th>
                    		@endforeach
                    	</tr>
                    @endforeach
                </tr>
            </tbody>
        </table>
        <button class="btn  btn-success">Salva</button>
    </div>

</form>
</div>
@push('bottom')
<script type="text/javascript">

</script>
<style>

</style>
@endpush
@endsection