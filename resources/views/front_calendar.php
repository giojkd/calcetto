<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/it.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
	<title>Risultati</title>
	<style type="text/css">

	body {
		font-size: 18px !important;
		font-family: 'Roboto Condensed', sans-serif;
	}


	a {
		color: #054c89 !important;
	}

	h4{
		font-size: 29px;
		font-weight: 600;
		color: #054c89 !important;
	}
</style>
</head>
<body class="pt-3">
	<div id="app">
		<div v-if="$route.name=='home'">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<a href="http://mspcalciofirenze.it"><img class="img-fluid mb-2" src="http://mspcalciofirenze.it/wp-content/uploads/2018/08/logo_msp-1.png"></a>
						
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<a class="btn btn-info mb-2" href="http://mspcalciofirenze.it"><i class="fa fa-chevron-circle-left"></i> Torna indietro</a>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="card mb-3">
							<div class="card-body">

								<form class="form" >
									<div class="row">
										<div class="col-md-4 col-xs-12 form-group">											
											<label>Manifestazione</label>
											<select class="form-control" name="tournaments_id" v-model="tournaments_id" v-on:change="load_rounds">
												<option value="">Scegli manifestazione...</option>
												<option v-for="tournament in tournaments" v-bind:value="tournament.id">
													{{ tournament.tournament_name }}
												</option>
											</select>
										</div>
										<div class="col-md-4 col-xs-12 form-group">
											<label>Girone</label>
											<select class="form-control" v-model="tournament_effective_rounds_id" v-on:change="load_teams">
												<option value="">Scegli girone...</option>
												<option v-for="round in rounds" v-bind:value="round.id">
													{{ round.round_label }}
												</option>
											</select>
										</div>
										<div class="col-md-4 col-xs-12 form-group">
											<label>Squadra</label>
											<select class="form-control" v-model="teams_id" v-on:change="load_results">
												<option value="">Scegli squadra...</option>
												<option v-for="team in teams" v-bind:value="team.teams_id">
													{{ team.team_name }}
												</option>
											</select>
										</div> 
									</div>


								</form></div>
							</div>
							<div v-if="tournaments_id != '' && tournament_effective_rounds_id!=''" >
								
								<div class="table-responsive text-center">
									
									<h4>Partite <a href="JavaScript:" onclick="print()"><i class="fa fa-print"></i></a></h4>
									
									<div v-for="(matches_by_day,turn) in results.matches_by_turn">
										<h1>Turno {{turn}}</h1>
										<div v-for="(matches,day) in matches_by_day">
											<h4>{{fedf(day)}}</h4>
											<div>
												<table class="table table-striped">
													<tbody>
														<tr v-for="match in matches">
															<td>{{match.field_name}}</td>
															<td>{{match.time_frame_start}}</td>
															<td>{{match.match_label}}</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<hr>
									</div>
								</div></div>
							</div>
						</div>
					</div>
				</div>
				<div v-if="$route.name=='team'" id="selected_team">

					<div class="container">
						<div class="row">
							<div class="col-12">
								<a class="btn btn-info" href="JavaScript:" onclick="window.history.back();"><i class="fa fa-chevron-circle-left"></i> Torna indietro</a>
								<h1>{{tsc.team.team_name}}</h1>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" id="home-tab" data-toggle="tab" href="#players-tab" role="tab" aria-controls="home" aria-selected="true">Giocatori</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" id="profile-tab" data-toggle="tab" href="#matches-tab" role="tab" aria-controls="profile" aria-selected="false">Calendario</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="tab-content">
							<div class=" tab-pane fade show active" id="players-tab">
								<div class="row">
									<div class="col-md-4 col-xs-6" v-for="player in tsc.players">
										<div class="card mb-3">
											<div class="card-body">
												<div class="row">
													<div class="col-md-4 col-xs-12 text-center">
														<i class="fa fa-user fa-4x"></i>
													</div>
													<div class="col-md-8 col-xs-12">
														{{player.player_full_name}}
														<table class="table table-striped table-hover">
															<tr>
																<td>Tessera</td>
																<td>{{player.player_licensee_number}}</td>
															</tr>
														</table>
													</div>
												</div>

											</div>
										</div>

									</div>
								</div>
							</div>
							<div class=" mt-3 tab-pane fade " id="matches-tab">
								<div class="row">
									<div class="col-12">
										<!--{{tsc.matches}}-->
										<div class="table-responsive">
											<table class="table table-hover table-striped">
												<thead>
													<tr>
														<th>Turno</th>
														<th>Partita</th>
														<th>Risultato</th>
														<th>Giorno</th>
														<th>Orario</th>
														<th>Campo</th>

													</tr>
												</thead>
												<tbody>
													<tr v-for="match in tsc.matches">
														<td>{{match.match_turn}}</td>
														<td>{{match.match_label}}</td>
														<td>{{match.teams_id_host_goals}}:{{match.teams_id_guest_goals}}</td>
														<td>{{fedf(match.match_day)}}</td>
														<td>{{match.time_frame_start}}</td>
														<td>{{match.field_name}}</td>

													</tr>
												</tbody>
											</table>
											<a href="JavaScript:" onclick="window.print();" class="btn btn-info">
												<i class="fa fa-print"></i>	Stampa
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>

			<!-- Optional JavaScript -->
			<!-- jQuery first, then Popper.js, then Bootstrap JS -->
			<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
			<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
			<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
			<script src="https://unpkg.com/vue-router@2.0.0/dist/vue-router.js"></script>

			<script type="text/javascript">

				const teamRouter = { template: '<div>Team {{$route.params.teams_id}}</div>' }

				var routes = [
				{ 
					path: '/teams/:teams_id', 
					name:'team'
				},
				{
					path:'/',
					name:'home'
				}
				]

				var router = new VueRouter({
  			routes // short for `routes: routes`
  		})



				var app = new Vue({
					router:router,

					el: '#app',
					data: {
						tournaments_id:'',
						tournament_effective_rounds_id:'',
						teams_id:'',
						tournaments: [],
						rounds:[],
						teams:[],
						results:{},
						tsc:{}
					},
					created(){
						fetch('/api/tournaments')
						.then(response=>response.json())
						.then(json=>{
							this.tournaments = json.data
						})
						this.load_results();

					},
					mounted(){
						console.log(this.$route.name);	
						if(this.$route.name=='team'){
							this.load_selected_team(this.$route.params.teams_id);
						}
					},
					watch:{
						$route (to, from){
							this.show = false;
						}
					},
					computed:{

					},
					methods:{
						fedf:function(date){
							return moment(date, 'YYYY-MM-DD').format('dddd DD/MM/YYYY');
						},
						load_selected_team:function(id){
							console.log(id);
							fetch('/front/team-selected/?teams_id='+id)
							.then(response=>response.json())
							.then(json=>{
								this.tsc = json
							})
						},
						load_results:function(){
							fetch('/front/get-stats?tournaments_id='+this.tournaments_id+'&tournament_effective_rounds_id='+this.tournament_effective_rounds_id+'&teams_id='+this.teams_id)
							.then(response=>response.json())
							.then(json=>{
								this.results = json
							})
						},
						load_rounds:function(event){
							if(this.tournaments_id!=''){

								fetch('/api/tournament_effective_rounds/?tournaments_id='+this.tournaments_id)
								.then(response=>response.json())
								.then(json=>{
									this.rounds = json.data
								})}else{
									this.rounds = []
								}
								this.load_results();
							},
							load_teams:function(event){
								if(this.tournament_effective_rounds_id!=''){

									fetch('/api/tournament_effective_round_teams/?tournament_effective_rounds_id='+this.tournament_effective_rounds_id)
									.then(response=>response.json())
									.then(json=>{
										this.teams = json.data
									})}else{
										this.teams = []
									}
									this.load_results();
								}
							}
						})
					</script>

				</body>
				</html>