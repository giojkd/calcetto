@extends('crudbooster::admin_template')
@section('content')
@push('bottom')
<style>
.match-team-result-input{
	width:40px;
	text-align: center;
}
.players-select{
	width:168px!important;
}
.players-input-group{
	width:280px;
}
.input-event-count{
	width: 50px!important;
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">

	function formatResultData (data) {
		if (!data.id) return data.text;
		if (data.element.selected) return
			return data.text;
	};

	$(function(){
		
		

		$('.event-type-form').submit(function(e){
			e.preventDefault();
			var form = $(this);
			var data = form.serialize();
			var action = form.attr('action');
			$.get(action,data,function(r){
				console.log(r);
			})
		})
	})
</script>
@endpush

<div class="table-responsive">
	<table class="table-hover table-striped table">
		<thead>
			<tr>
				@foreach($event_types_desc as $event_type)
				<th class="text-center" title="{{$event_type->event_type_name}}" style="color:{{$event_colors[$event_type->id]}}">
					<i class="fa {{$event_type->event_type_icon}}"></i>
				</th>
				@endforeach
				<th class="text-center">Goal</th>
				<th class="text-center">Squadra in casa</th>
				<th class="text-center">Squadra fuori casa</th>
				<th class="text-center">Goal</th>
				@foreach($event_types_asc as $event_type)
				<th class="text-center" title="{{$event_type->event_type_name}}" style="color:{{$event_colors[$event_type->id]}}">
					<i class="fa {{$event_type->event_type_icon}}"></i>
				</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			@foreach($matches as $match)
			<tr>
				@foreach($event_types_desc as $event_type)
				<td>
					<form class="event-type-form" action="/admin/matches/edit-match-event-by-type">
		<input type="hidden" name="matches_id" value="{{$match->matches_id}}">
		<input type="hidden" name="match_event_types_id" value="{{$event_type->id}}">
		<input type="hidden" name="teams_id" value="{{$match->teams_id_host}}">
		<?php 
		$event_players = $events[$match->matches_id][$match->teams_id_host][$event_type->id];
		if(empty($event_players)){
			$event_players = [['players_id'=>0,'events'=>0]];
		}
		?>

		@foreach($event_players as $player_id =>  $event_player)
		<div class="form-group from-group-sm">
			<div class="input-group players-input-group">

				<select 
				onchange="
						$(this)
						.closest('.input-group')
						.find('.input-event-count')
						.attr('name','players_id['+$(this).val()+']');
						$(this).closest('form').submit();
						"
				class="form-control players-select">
				<option value="">Scegli...</option>
				@foreach($match->team_host_players as $player)
				<option 
				value="{{$player->id}}"
				<?php

				if(!is_null($event_players[$player->id])){
					echo ($player->id==$player_id) ? 'selected' : '' ;
				}
				?>
				>
				
				{{$player->player_full_name}}
				</option>
				@endforeach
				</select>

				<!--quanti eventi-->

				<input 
				onblur="$(this).closest('form').submit();" 
				type="number" 
				min="0" 
				value="{{$event_player['events']}}" 
				name="players_id[{{$player_id}}]"
				class="form-control input-event-count">

				<!--quanti eventi-->

				<div class="input-group-btn">
						<button type="button" class="btn btn-success btn-sm" 
						onclick='
						$(this).closest("div.form-group").clone().appendTo($(this).closest("form"));
						$(this).closest("form").submit();
						'>
							<i class="fa-plus fa"></i>
						</button>
						<button 
							onclick='
							$(this).closest(".form-group").find("input").val(0);
							$(this).closest("form").submit();
							$(this).closest(".form-group").remove();
							' 
							type="button" class="btn btn-danger btn-sm">
								<i class="fa-minus fa"></i>
						</button>
					</div>
				</div>
			</div>
		@endforeach
</form>
					
</td>
@endforeach
<td>
	<form class="event-type-form" action="/admin/matches/set-match-team-score">
		<input type="hidden" name="matches_id" value="{{$match->matches_id}}">
		<input type="hidden" name="teams_id" value="{{$match->teams_id_host}}">
		<input type="hidden" name="teams_type" value="host">
		<input onblur="$(this).closest('form').submit()" name="goals" value="{{$match->teams_id_host_goals}}" class="form-control match-team-result-input">
	</form>
</td>
<td>{{$match->team_host_name}}</td>
<td>{{$match->team_guest_name}}</td>
<td>

	<form class="event-type-form" action="/admin/matches/set-match-team-score">
		<input type="hidden" name="matches_id" value="{{$match->matches_id}}">
		<input type="hidden" name="teams_id" value="{{$match->teams_id_guest}}">
		<input type="hidden" name="teams_type" value="guest">
		<input onblur="$(this).closest('form').submit()" name="goals" value="{{$match->teams_id_guest_goals}}" class="form-control match-team-result-input">
	</form>
</td>
@foreach($event_types_asc as $event_type)
<td>
	<form class="event-type-form" action="/admin/matches/edit-match-event-by-type">
		<input type="hidden" name="matches_id" value="{{$match->matches_id}}">
		<input type="hidden" name="match_event_types_id" value="{{$event_type->id}}">
		<input type="hidden" name="teams_id" value="{{$match->teams_id_guest}}">
		<?php 
		$event_players = $events[$match->matches_id][$match->teams_id_guest][$event_type->id];
		if(empty($event_players)){
			$event_players = [['players_id'=>0,'events'=>0]];
		}
		?>

		@foreach($event_players as $player_id =>  $event_player)
		<div class="form-group from-group-sm">
			<div class="input-group players-input-group">

				<select 
				onchange="
						$(this)
						.closest('.input-group')
						.find('.input-event-count')
						.attr('name','players_id['+$(this).val()+']');
						$(this).closest('form').submit();
						"
				class="form-control players-select">
				<option value="">Scegli...</option>
				@foreach($match->team_guest_players as $player)
				<option 
				value="{{$player->id}}"
				<?php

				if(!is_null($event_players[$player->id])){
					echo ($player->id==$player_id) ? 'selected' : '' ;
				}
				?>
				>
				
				{{$player->player_full_name}}
				</option>
				@endforeach
				</select>

				<!--quanti eventi-->

				<input 
				onblur="$(this).closest('form').submit();" 
				type="number" 
				min="0" 
				value="{{$event_player['events']}}" 
				name="players_id[{{$player_id}}]"
				class="form-control input-event-count">

				<!--quanti eventi-->

				<div class="input-group-btn">
						<button type="button" class="btn btn-success btn-sm" 
						onclick='
						$(this).closest("div.form-group").clone().appendTo($(this).closest("form"));
						$(this).closest("form").submit();
						'>
							<i class="fa-plus fa"></i>
						</button>
						<button 
							onclick='
							$(this).closest(".form-group").find("input").val(0);
							$(this).closest("form").submit();
							$(this).closest(".form-group").remove();
							' 
							type="button" class="btn btn-danger btn-sm">
								<i class="fa-minus fa"></i>
						</button>
					</div>
				</div>
			</div>
		@endforeach
</form>
</td>
@endforeach
</tr>
@endforeach
</tbody>
</table>
</div>
@endsection
