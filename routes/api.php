<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::get('get-games','FieldReservation@getGames');
Route::get('get-fields','FieldReservation@getFields2');
Route::post('new-field-reservation','FieldReservation@newFieldReservation');

Route::get('get-match-details/{id}','AdminMatchesController@getMatchDetails');
