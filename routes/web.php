<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function () {
	return view('welcome');
});

Route::get('send-today-reminders','AdminCommunications38Controller@sendTodayReminders');

Route::get('mail_test_1', function () {
	return view('mail_test_1');
});
Route::get('mail_test_2', function () {
	return view('mail_test_2');
});

Route::get('mail_test_3', function () {
	return view('mail_test_3');
});

Route::get('front',function(){
	return view('front');
});

Route::get('front-only-matches',function(){
	return view('front_only_matches');
});

Route::get('front-calendar',function(){
	return view('front_calendar');
});

Route::get('front/teams',function(){
	return view('front_teams');
});

Route::get('front/book-your-field',function(){
	return view('book_your_field');
});

#Route::get('bulk-results-updater','AdminMatchesController@bulkResultsUpdater');

Route::get('front/matches',function(){

	$fromDay = date('Y-m-d');
	$toDay = date('Y-m-d',strtotime('+90days'));

	$matches = DB::table('matches')
	    		->orderBy('match_day','ASC')
	    		->leftJoin('fields','matches.fields_id','=','fields.id')
	    		->leftJoin('time_frames','matches.time_frames_id','=','time_frames.id')
	    		->leftJoin('tournament_effective_rounds','matches.tournament_effective_rounds_id','=','tournament_effective_rounds.id')
	    		->leftJoin('tournaments','tournament_effective_rounds.tournaments_id','=','tournaments.id')

		    	->leftJoin('referees','referees.id','=','matches.referees_id')
	    		->leftJoin('delegates','delegates.id','=','matches.delegates_id')
	    		->whereBetween('matches.match_day',[$fromDay,$toDay])
					->where('tournaments.tournament_status',1)
	    		->orderBy('field_name','ASC')
	    		->orderBy('time_frames_id','ASC')
	    		->get();

	    		$matches_indexed = [];
	    		$fields = [];
	    		$days = [];
	    		if(count($matches)){
	    			foreach($matches as $match){
	    				$day = $days_of_the_week[date('w',strtotime($match->match_day))].' '.date('d/m/Y',strtotime($match->match_day));
	    				$matches_indexed[$day][$match->field_name][] = $match;
	    				$days[] = $match->match_day;
	    			}
	    		}
	    		$days = array_unique($days);

	return view('mail_test_4',[
	    			'match_groups' => $matches_indexed,
	    			'matches_from' => $days_of_the_week[date('w',strtotime($fromDay))].' '.date('d/m/Y',strtotime($fromDay)),
	    			'matches_to' => $days_of_the_week[date('w',strtotime($toDay))].' '.date('d/m/Y',strtotime($toDay)),
	    		]);

});

/*
Route::get('front/matches',function(){
	return view('matches');
});

Route::get('front/get-matches',function(){
	return[
		'matches' => DB::table('matches')
		->select(
			'matches.*',
			DB::raw('(SELECT referee_full_name FROM referees WHERE referees.id = matches.referees_id) as referee'),
			DB::raw('(SELECT delegate_full_name FROM delegates WHERE delegates.id = matches.delegates_id) as delegate'),
			DB::raw('(SELECT field_name FROM fields WHERE fields.id = matches.fields_id) as field'),
			DB::raw('(SELECT time_frame_start FROM time_frames WHERE time_frames.id = matches.time_frames_id) as time')
		)
		->where('match_day','>',date('Y-m-d'))
		->take(75)
		->orderBy('match_day','ASC')
		->orderBy('fields_id','ASC')
		->orderBy('time_frames_id','ASC')
		->get()
	];
});
*/
Route::get('front/get-teams',function(){
	return [
			'teams'=>DB::table('teams')->orderBy('team_name')->get(),
			'players'=>DB::table('players')->select('players.*',DB::raw('(SELECT GROUP_CONCAT(team_name) FROM teams LEFT JOIN teams_players ON
				teams.id = teams_players.teams_id WHERE teams_players.players_id = players.id GROUP BY players_id) as teams'))->orderBy('player_full_name')->get(),
			];
});

Route::get('front/team-selected',function(){
	$team = DB::table('teams')->where('id',$_GET['teams_id'])->first();
	$players = DB::table('teams_players')->leftJoin('players','players.id','=','teams_players.players_id')->where('teams_players.teams_id',$_GET['teams_id'])->get();
	$matches =
	DB::table('matches_teams')
	->leftJoin('matches','matches.id','=','matches_teams.matches_id')
	->leftJoin('fields','fields.id','=','matches.fields_id')
	->leftJoin('time_frames','time_frames.id','=','matches.time_frames_id')
	->leftJoin('tournament_effective_rounds','tournament_effective_rounds.id','=','matches.tournament_effective_rounds_id')
	->leftJoin('tournaments','tournaments.id','=','tournament_effective_rounds.tournaments_id')
	->where('matches_teams.teams_id',$_GET['teams_id'])
	->where('tournaments.tournament_status',1)
	->whereNotNull('match_label')
	->orderBy('match_day','ASC')
	->get();
	$data['team'] = $team;
	$data['players'] = $players;
	$data['matches'] = $matches;
	return $data;
});

Route::get('front/get-stats',function(){


	function sortBy($field, &$array, $direction = 'asc')
	{
	    usort($array, create_function('$a, $b', '
	        $a = $a->'.$field.';
	        $b = $b->'.$field.';

	        if ($a == $b) return 0;

	        $direction = strtolower(trim($direction));

	        return ($a ' . ($direction == 'desc' ? '>' : '<') .' $b) ? -1 : 1;
	    '));

	    return true;
	}

	$data = [];
	$round = DB::table('tournament_effective_rounds')->where('id',$id)->first();
	$tournament = DB::table('tournaments')->where('id',$round->tournaments_id)->where('tournament_status',1)->first();
	$data['page_title'] = 'Dettaglio del girone '.$round->round_label. ' / '.$tournament->tournament_name ;

	$matches_ =
	DB::table('matches')
	->select('*')
	->addSelect('matches.id as matches_id')
	->leftJoin('matches_teams','matches.id','=','matches_teams.matches_id')
	->leftJoin('teams','teams.id','=','matches_teams.teams_id')
	->leftJoin('tournament_effective_rounds','tournament_effective_rounds.id','=','matches.tournament_effective_rounds_id')
	->leftJoin('tournaments','tournaments_id','=','tournament_effective_rounds.tournaments_id')
	#->leftJoin('match_events','matches.id','=','match_events.matches_id')
	->orderBy('match_day','ASC')
	->groupBy('matches.id')
	->groupBy('teams.id')
	->where('tournament_status',1);


	if($_GET['teams_id']>0){
		$matches_->where('matches_teams.teams_id',$_GET['teams_id']);
	}

	if($_GET['tournaments_id']>0 && $_GET['tournament_effective_rounds_id']>0){
		$matches_->where('tournaments_id',$_GET['tournaments_id']);
		$matches_->where('tournament_effective_rounds_id',$_GET['tournament_effective_rounds_id']);
		$matches_ = $matches_->get();
	}else{
		exit;
	}






	if(count($matches_)){
		foreach($matches_ as $match){
			if(!isset($teams[$match->teams_id]))
				$teams[$match->teams_id] = new stdClass();
			$teams[$match->teams_id]->teams_id = $match->teams_id;
			$teams[$match->teams_id]->team_name = $match->team_name;
			$teams[$match->teams_id]->team_logo = $match->team_logo;
			$teams[$match->teams_id]->points += $match->points;


			if($match->result=='win'){
				$teams[$match->teams_id]->win ++;
				$teams[$match->teams_id]->played++;
			}
			if($match->result=='lose'){
				$teams[$match->teams_id]->lose ++;
				$teams[$match->teams_id]->played++;
			}
			if($match->result=='draw'){
				$teams[$match->teams_id]->draw ++;
				$teams[$match->teams_id]->played++;
			}
			$teams[$match->teams_id]->scores_in += $match->scores_in;
			$teams[$match->teams_id]->scores_out += $match->scores_out;
			$teams[$match->teams_id]->scores_diff+=$match->scores_out;
			$teams[$match->teams_id]->scores_diff-=$match->scores_in;
		}
	}

	#echo '<pre>';
	#print_r($teams);

#	sortBy('points',   $teams, 'desc');
#	sortBy('scores_diff',   $teams, 'desc');

array_multisort(array_column($teams, 'points'),  SORT_DESC,
                array_column($teams, 'scores_diff'), SORT_DESC,
                $teams);

#	$teams = array_orderby($teams, 'points', SORT_DESC, 'scores_diff', SORT_DESC);

	$data['teams'] = $teams;

	$events_ =
	DB::table('match_events')
	->select('match_events.*','players.*',DB::raw('(SELECT GROUP_CONCAT(team_name) FROM teams LEFT JOIN teams_players ON teams.id = teams_players.teams_id WHERE teams_players.players_id = players.id GROUP BY teams_players.players_id ) as team_name'))
	->leftJoin('matches','matches.id','=','match_events.matches_id')
	->leftJoin('players','players.id','=','match_events.players_id')
	->leftJoin('match_event_types','match_event_types.id','=','match_events.match_event_types_id')
	->leftJoin('tournament_effective_rounds','tournament_effective_rounds.id','=','matches.tournament_effective_rounds_id')
	->leftJoin('tournaments','tournaments_id','=','tournament_effective_rounds.tournaments_id')
	->groupBy('match_events.id');

	if($_GET['tournaments_id']>0){
		$events_->where('tournaments_id',$_GET['tournaments_id']);
	}




	if($_GET['tournament_effective_rounds_id']>0){
		$round_is_final_phase = DB::table('tournament_effective_rounds')->where('id', $_GET['tournament_effective_rounds_id'])->pluck('final_phase_round');

		if($round_is_final_phase[0] != 1){
				$events_->where('tournament_effective_rounds_id',$_GET['tournament_effective_rounds_id']);
		}

	}
	if($_GET['teams_id']>0){
		$events_->where('match_events.teams_id',$_GET['teams_id']);
	}

	$events_ = $events_->get();

	#$data['events'] = $events_;

	if(count($events_)){
		foreach($events_ as $event){
			if(!isset($players[$event->players_id]))
				$players[$event->players_id] = new stdClass();
			$players[$event->players_id]->player_full_name = $event->player_full_name;
			if($event->match_event_types_id==1)
				$players[$event->players_id]->goals ++;
			if($event->match_event_types_id==2)
				$players[$event->players_id]->yellow_cards ++;
			if($event->match_event_types_id==3)
				$players[$event->players_id]->red_cards ++;
			if($event->match_event_types_id==4)
				$players[$event->players_id]->yellow_red_cards ++;
			$players[$event->players_id]->team_name = $event->team_name;
		}
	}

	$players = (is_array($players)) ? $players : [] ;

	sortBy('goals',$players,'desc');

	$data['players'] = $players;

	$matches_ =
	DB::table('matches')
	->select('*')
	->addSelect('matches.id as matches_id')
	->leftJoin('fields','fields.id','=','matches.fields_id')
	->leftJoin('time_frames','time_frames.id','=','matches.time_frames_id')
	->leftJoin('referees','referees.id','=','matches.referees_id')
	->leftJoin('delegates','delegates.id','=','matches.delegates_id')
	->leftJoin('tournament_effective_rounds','tournament_effective_rounds.id','=','matches.tournament_effective_rounds_id')
	->leftJoin('tournaments','tournaments_id','=','tournament_effective_rounds.tournaments_id')
	->whereNotNull('match_day')
	->orderBy('match_day');

	if($_GET['tournaments_id']>0){
		$matches_->where('tournaments.id',$_GET['tournaments_id']);
	}
	if($_GET['tournament_effective_rounds_id']>0){
		$matches_->where('tournament_effective_rounds.id',$_GET['tournament_effective_rounds_id']);
	}
	if($_GET['teams_id']>0){
		$matches_->leftJoin('matches_teams','matches.id','=','matches_teams.matches_id');
		$matches_->where('matches_teams.teams_id',$_GET['teams_id']);
	}




	$matches_ = $matches_->get();

	if(!empty($matches_)){
		foreach($matches_ as $match){
			$matches__[$match->match_turn][$match->match_day][] = $match;
		}
	}


	$matches = $matches_;

	$data['matches_by_turn'] = $matches__;
	$data['matches'] = $matches;


	return $data;
});
